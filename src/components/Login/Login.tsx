import React, { FC } from 'react'
import styles from './Login.module.sass'
import '../../styles/auth.sass'
import { trans } from '../../translations'
import LoginForm from './LoginForm'

const Login: FC = () => {
  return (
    <div className="auth-root">
      <div className={`auth-image ${styles.loginImg}`} />
      <div className={`form-cover ${styles.loginForm}`}>
        <div>
          <h1 className="form-title">{trans('auth.sign.in.title')}</h1>
          <LoginForm/>
        </div>
      </div>
    </div>
  )
}

export default Login