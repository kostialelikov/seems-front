import React, { FC, useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import FormHelperText from '@material-ui/core/FormHelperText';
import styles from './LoginForm.module.sass'
import Input from '../common/Input'
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { trans } from '../../translations'
import {Field, FormErrors, InjectedFormProps, SubmissionError, reduxForm} from 'redux-form'
import useAuth, { LoginFormData } from '../../hooks/useAuth'
import isValidEmail from '../../utils/isValidEmail'
import {Redirect} from 'react-router-dom';

const LoginForm: FC<InjectedFormProps<LoginFormData, any, any>> = (props) => {
  const auth = useAuth()
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    const handleUserKeyPress = (event: any) => {
      const { keyCode } = event;

      if (keyCode === 13) {
        handleSignIn()
      }
    };

    window.addEventListener('keydown', handleUserKeyPress);
    return () => {
      window.removeEventListener('keydown', handleUserKeyPress);
    };
  }, []);

  const handleSignIn = props.handleSubmit((formData: LoginFormData) => {
    const errors = validate(formData);
    if (errors){
      throw new SubmissionError(errors)
    }
    return auth.login(formData.email, formData.password)
  })

  const handleFieldChange = (e: any) => {
    const {clearSubmitErrors, change} = props as any
    const name = e.target && e.target.name
    const value = e.target && e.target.value && e.target.value.trim()
    change(name, value)
    e.preventDefault()
    return clearSubmitErrors && clearSubmitErrors()
  }

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  }

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const {error, submitFailed} = props

  return (
    <div className="form-control">
      <div className={`auth-form ${styles.loginForm}`}>
        <Field name="email" component={Input} fullWidth
               placeholder={trans('auth.placeholder.login')}
               disabled={props.submitting}
               onChange={handleFieldChange}
        />
        <Field name="password" component={Input} fullWidth
               type={showPassword ? 'text' : 'password'}
               placeholder={trans('auth.placeholder.password')}
               disabled={props.submitting}
               onChange={handleFieldChange}
               endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    color="secondary"
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {!showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
        />

        <div className="error-wrapper">
          {(error && submitFailed) && (
            <FormHelperText error>{trans(error)}</FormHelperText>
          )}
        </div>
      </div>

      <Button type="submit" variant="contained" color='secondary'
              disabled={props.submitting}
              onClick={handleSignIn}>
        {trans('auth.sign.in.button')}
      </Button>
    </div>
  )
}

const validate = (values: LoginFormData): FormErrors<LoginFormData, any> | null => {
  const {
    email,
    password
  } = values

  if (!email) {
    return {_error: trans('error.email.password.incorrect')}
  }

  if (!password) {
    return {_error: trans('error.email.password.incorrect')}
  }

  if (!isValidEmail(email)) {
    return {_error: trans('error.email.password.incorrect')}
  }

  return null
}

export default reduxForm<LoginFormData, any, any>({
  form: 'LoginForm'
})(LoginForm)