import React, { FC } from 'react'
import '../../../../../../styles/dashboard.sass'
import './StatusLabel.module.sass'
import styles from './StatusLabel.module.sass'
import {trans} from '../../../../../../translations'

interface Props {
  status: string // one of delete || edit || create
}

export const StatusLabel: FC <Props> = (props) => {
  const { status } = props;
  const label = trans(`status.label.${status}`);

  return (
    <div className={styles.root}>
      <div className={`${styles.icon} ${styles[`icon-${status}`]}`}/>
      <p>{label}</p>
    </div>
  )
};