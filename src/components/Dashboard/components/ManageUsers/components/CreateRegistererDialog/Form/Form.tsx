import React, {FC} from 'react'
import {trans} from "../../../../../../../translations"
import TextField from '../../../../../../common/TextField'
import isValidEmail from "../../../../../../../utils/isValidEmail";
import {Field, FormErrors, InjectedFormProps, reduxForm} from "redux-form";
import styles from "./From.module.sass";
import FormHelperText from "@material-ui/core/FormHelperText";
import Button from "@material-ui/core/Button";
import {PER_PAGE} from "../../../../../../../data/constants";
import useUser from "../../../../../../../hooks/useUser";

interface Props {
  handleClose: () => void
}

interface FormData {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

const Form: FC<InjectedFormProps<FormData, any, any> & Props> = (props) => {

  const {createRegisterer, getUsers, page, search, perPage} = useUser();

  const handleAddAgency = props.handleSubmit((formData: FormData) => {
    return createRegisterer(formData).then(() => {
      return getUsers({
        perPage: perPage || PER_PAGE,
        page,
        search
      });
    }).then(handleClose).catch(console.error);
  });

  const handleFieldChange = () => {
    const {clearSubmitErrors} = props as any

    return clearSubmitErrors && clearSubmitErrors()
  }

  const {error, submitFailed, handleClose, submitting} = props

  return (
    <div className="dialog-form">
      <div className="flexed-row">
        <div className="half-column">
          <Field
            component={TextField}
            name={'firstName'}
            label={trans('input.label.owner.first.name')}
            placeholder={trans('input.placeholder.please.enter')}
            onChange={handleFieldChange}
          />
        </div>
        <div className="half-column">
          <Field
            component={TextField}
            name={'lastName'}
            label={trans('input.label.owner.last.name')}
            placeholder={trans('input.placeholder.please.enter')}
            onChange={handleFieldChange}
          />
        </div>
      </div>
      <div className="flexed-row">
        <Field
          component={TextField}
          name={'email'}
          label={trans('input.label.owner.email')}
          placeholder={trans('input.placeholder.please.enter')}
          onChange={handleFieldChange}
        />
      </div>
      <div className="flexed-row">
        <Field
            component={TextField}
            name={'password'}
            label={trans('input.label.password')}
            placeholder={trans('input.placeholder.please.enter')}
            onChange={handleFieldChange}
        />
      </div>
      <div className={`${styles.errorWrapper}`}>
        {(error && submitFailed) && (
          <FormHelperText error>{trans(error)}</FormHelperText>
        )}
      </div>
      <div className="dialog-action">
        <Button
          color='secondary'
          variant='contained'
          disabled={submitting}
          onClick={handleAddAgency}
        >{trans('button.create')}</Button>
      </div>
    </div>
  )
}

const validate = (values: FormData): FormErrors<FormData, any> => {
  const {
    email,
    firstName,
    lastName,
    password
  } = values;

  const errors: any = {};

  if (!(email && email.trim())) {
    errors.email = trans('errors.field.required')
      .replace('{field}', trans('input.label.owner.email'))
  }

  if (!(firstName && firstName.trim())) {
    errors.firstName = trans('errors.field.required')
      .replace('{field}', trans('input.label.owner.first.name'))
  }

  if (!(lastName && lastName.trim())) {
    errors.lastName = trans('errors.field.required')
      .replace('{field}', trans('input.label.owner.last.name'))
  }

  if (!(password && password.trim())) {
    errors.password = trans('errors.field.required')
      .replace('{field}', trans('input.label.password'))
  }

  if (email && !isValidEmail(email)) {
    errors.email = trans('errors.invalid.email')
  }

  return Object.keys(errors).length !== 0 ? errors : null;
}


export default reduxForm<FormData, any, any>({
  form: 'CreateRegistererForm',
  validate,

  asyncBlurFields: ['email']
})(Form);
