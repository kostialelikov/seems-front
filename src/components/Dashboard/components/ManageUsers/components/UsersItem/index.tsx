import React, {FC} from 'react'
import '../../../../../../styles/dashboard.sass'
import {
    TableCell,
    TableRow,
    IconButton,
    TableBody
} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import detailsIcon from '../../../../../../assets/icons/ico-details.svg'
import moment from "moment";
import {UserInterface} from "../../../../../../interfaces/user";
import createUserAvatarFromName from '../../../../../../utils/createUserAvatarFromName'
import {Link} from 'react-router-dom';
import {StatusLabel} from '../StatusLabel';


/*
 Action Log Page layout the same for User/Admin/Owner
 The visual difference is the Title text
    For Admin and Owner: title='app.bar.title.actions.log'
    For User:  title='app.bar.title.my.actions.log'
*/

interface Props {
    user: UserInterface
    // handleActions: () => void
}

const UsersItem: FC<Props> = (props) => {
    const {user} = props;

    // eslint-disable-next-line no-restricted-globals
    const firstName = user.firstName, lastName = user.lastName, email = user.email;
    const createdAt = user.createdAt ? moment(user.createdAt).format('MMMM DD, YYYY HH:MM') : "-";


    return (

        <TableRow>
            <TableCell
                className='width-4'
            >

                <div className='user_with_avatar'>
          <span className='user_avatar_table'>
            {
                <Avatar alt={user.firstName}>{createUserAvatarFromName({firstName, lastName})}</Avatar>
            }

          </span>
                    <div className="cell-overflow">{user.firstName} {user.lastName}</div>
                </div>

            </TableCell>
            <TableCell className='width-3'>
                <div className="cell-overflow">{createdAt}</div>
            </TableCell>
            <TableCell className='width-4'>
                <div className="cell-overflow">{email}</div>
            </TableCell>
            <TableCell className='width-0'>
                <div className='one-action'>
                    <Link to={`/dashboard/users/${user._id}`}>
                        <IconButton>
                            <img src={detailsIcon} alt='detailsIcon'/>
                        </IconButton>
                    </Link>
                </div>
            </TableCell>
        </TableRow>
    );
};

export default UsersItem;
