import React, {FC, useEffect, useRef, useState} from 'react'
import {
    AppBar,
    AppBarBefore,
    AppBarAfter,
    Pagination
} from '../../../common/AppBar'
import {THead, TBody} from '../../../common/Table'
import {Button, TableCell, TableRow} from '@material-ui/core'
import {trans} from '../../../../translations'
import SearchField, {SearchFieldRef} from '../../../common/SearchField'
import useUser from "../../../../hooks/useUser";
import UsersItem from './components/UsersItem/index'
import {PER_PAGE} from '../../../../data/constants';
import Loading from "../../../common/Loading";
import PageField, {PageFieldRef} from "../../../common/PageField";
import {CreateRegistererDialog} from './components/CreateRegistererDialog/CreateRegistererDialog';

export const ManageUsers: FC = () => {
    const [loading, setLoading] = useState(false);
    const [registerer, setRegisterer] = useState(false);

    const {page, perPage, search, getUsers, users = [], total} = useUser();

    const searchRef = useRef<SearchFieldRef>(null);
    const pageRef = useRef<PageFieldRef>(null)

    const handleRegisterer = () => {
      setRegisterer(!registerer);
    };

    useEffect(() => {
        setLoading(true);
        getUsers({
            perPage: perPage || PER_PAGE,
            page,
            search,
        }).catch(console.error).then(() => setLoading(false));
    }, []);

    const pageChanged = (nextPage: number) => {
        setLoading(true);
        getUsers({
            page: nextPage,
            perPage: perPage || PER_PAGE,
            search: search,
        }).catch(console.error).then(() => setLoading(false));
    };

    const handleSearchChange = (search: string) => {
        setLoading(true);
        getUsers({
            page: 1,
            perPage: perPage || PER_PAGE,
            search: search
        }).catch(console.error).then(() => setLoading(false));
    };

    return (
        <div className="dashboard-content">
            <Loading loading={loading}/>
            <AppBar title="menu.item.users">
                <AppBarBefore>
                    <SearchField ref={searchRef} defaultValue={search} onChange={handleSearchChange}/>
                    <Button color="secondary" onClick={() => handleRegisterer()}>Add Registerer</Button>
                </AppBarBefore>
                <AppBarAfter>
                    <Pagination currentPage={page!} perPage={perPage!}
                                totalPages={Math.ceil((total || 1) / (perPage || PER_PAGE))} handleChange={pageChanged}/>
                </AppBarAfter>
            </AppBar>

            <THead>
                <TableRow>

                    <TableCell className='width-4' align="left">
                        {trans('table.header.user')}
                    </TableCell>
                    <TableCell className='width-3' align="left">
                        {trans('table.header.date')}
                    </TableCell>
                    <TableCell className='width-4'>
                        {trans('table.header.email')}
                    </TableCell>

                </TableRow>
            </THead>

            <TBody>
                {
                    users.map(u => (<UsersItem key={u._id} user={u}/>))
                }
            </TBody>
            <CreateRegistererDialog open={registerer} handleClose={handleRegisterer} />
        </div>
    );
};
