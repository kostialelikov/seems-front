import React, { FC } from 'react'
import clsx from 'clsx';
import {
  createStyles,
  Theme,
  makeStyles } from '@material-ui/core/styles/index'
import Drawer from '@material-ui/core/Drawer'
import Button from '@material-ui/core/Button'
import { colors, fontSizes } from '../../../../theme'
import hideIcon from '../../../../assets/icons/ico-hide.svg'
import { SideMenu } from '../SideMenu/SideMenu'
import {trans} from "../../../../translations";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      flexShrink: 0,
      width: drawerWidth,
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: 80,
      [theme.breakpoints.up('sm')]: {
        width: 80,
      },
    },
    paper:{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      background: colors.darkViolet,
      border: 'none'
    },
    hideBtn:{
      border: 'none',
      borderRadius: 0,
      alignItems: 'center',
      justifyContent: 'flex-start',
      padding: '19px 34px 15px',
      background: 'rgba(0, 0, 0, 0.08)',
      '& > span':{
        color: '#e6f4f6',
        fontWeight: 'normal',
        fontSize: fontSizes.table,
       '& > img':{
          marginRight: 19
       }
      },
      '&:hover': {
        background: 'rgba(0, 0, 0, 0.12)',
      }
    },
    hideBtnSmall:{
      height: 60,
      paddingLeft: 30,
      paddingRight: 10,
      '& > span > span':{
        display: 'none'
      },
      '& span > img': {
        transform: 'rotate(180deg)'
      }
    }
  })
);

interface Props {
  open: boolean,
  setOpen: (value: boolean) => void
}

const MenuDrawer: FC<Props> = (props) => {
  const classes = useStyles();
  const {open, setOpen} = props;
  const [width, setWidth] = React.useState(window.innerWidth);
  const [variant, setVariant] = React.useState(window.innerWidth > 950 ? 'permanent' : 'temporary');

  const handleResize = () => {
    if (window.innerWidth <= 950 && width > 950) {
      setVariant('temporary');
    } else if (window.innerWidth > 950 && width <= 950) {
      setVariant('permanent')
    }
    setWidth(window.innerWidth)
  };

  React.useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  });

  const handleDrawerToggle = () => {
    setOpen(!open);
  };

  return (
    <Drawer
      onClose={() => setOpen(false)}
      variant={variant as any}
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: open,
        [classes.drawerClose]: !open,
      })}
      classes={{
        paper: clsx(classes.paper, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })
      }}
      open={open}
    >
      <SideMenu open={open}/>
      { variant === 'permanent' &&
        <Button
          variant="contained"
          onClick={handleDrawerToggle}
          className={clsx(classes.hideBtn, {
            [classes.hideBtnSmall]: !open,
          })}
        >
          <img src={hideIcon} alt="hide"/>
          {open && <span>{trans('menu.button.hide')}</span>}
        </Button>
      }
    </Drawer>
  )
}

export default MenuDrawer