import React, { FC } from 'react'
import useAuth from '../../../../hooks/useAuth'
import Button from '@material-ui/core/Button'
import { getMenuLinks } from './menuLinks'
import { trans } from '../../../../translations'
import styles from './SideMenu.module.sass'
import { UserMenu } from './components/UserMenu/UserMenu'
import MenuItem from './components/MenuItem/MenuItem'
import logoutIcon from '../../../../assets/icons/ico-logout.svg'
import { UserInterface } from '../../../../interfaces/user'

interface Props {
  open: boolean
}

export const SideMenu: FC<Props> = (props) => {
  const { open } = props
  const { logout, user } = useAuth();
  const menuLinks = getMenuLinks(user)
  return (
    <div className={styles.sideMenu}>
      <div className={styles.menu}>
        <UserMenu open={open} user={user as UserInterface}/>
        <ul className={styles.menuList}>
          {
            menuLinks.map((item: any, key: number) => {
              return (
                <MenuItem
                  key={key}
                  path={item.path}
                  label={item.label}
                  menuIcon={item.icon}
                  open={open}
                />
              )
            })
          }
        </ul>

        <div className={`${styles.logoutBtn} ${!open ? styles.small : ''}`}>
          <Button variant="outlined" color="primary" onClick={logout}>
            <img src={logoutIcon} alt="logout"/>
            {open && <span>{trans('menu.button.logout')}</span>}
          </Button>
        </div>
        {/*
          open &&
          <div className={styles.terms}>
            <p>{trans('copyright')}</p>
            <Link to='/dashboard/terms'>{trans('terms.and.conditions')}</Link>
          </div>
        */}
      </div>
    </div>
  )
}
