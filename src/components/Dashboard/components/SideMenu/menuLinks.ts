import users from '../../../../assets/icons/ico-user.svg';

export const getMenuLinks: any = (user: any) => {
  let menuLinks = [];

  if (['admin'].includes(user.role || '')) {
    menuLinks.push({
      path: '/dashboard/users',
      label: 'menu.item.users',
      icon: users
    });
  }

  if (['admin', 'registerer'].includes(user.role || '')){
    menuLinks.push({
      path: '/dashboard/sremps',
      label: 'menu.item.quotes',
      icon: users
    })
  }

  if (['admin', 'registerer', 'user'].includes(user.role || '')){
    menuLinks.push({
      path: '/dashboard/read_requests',
      label: 'menu.item.read.requests',
      icon: users
    })
  }

  return menuLinks;
};
