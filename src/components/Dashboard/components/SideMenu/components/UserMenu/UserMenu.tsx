import React, {FC} from 'react'
import {Link} from 'react-router-dom'
import styles from './UserMenu.module.sass'
import {UserInterface} from '../../../../../../interfaces/user'
import createUserAvatarFromName from '../../../../../../utils/createUserAvatarFromName';

interface Props {
    user: UserInterface
    open: boolean
}

export const UserMenu: FC<Props> = (props) => {
    const {open, user} = props;
    const {firstName = '', lastName = '', email} = user

    return (
        <div className={styles.userMenu}>
            <div className={styles.userAvatar}>
                {
                    <div>
              <span className={styles.userCreateAvatar}>
                {createUserAvatarFromName({firstName: firstName, lastName: lastName})}
              </span>
                    </div>
                }
            </div>
            {open && user &&
            <Link to="/dashboard/settings" className={styles.title}>
                <div title={`${firstName} ${lastName}`}>
                    {firstName} {lastName}
                </div>
                <div title={email}>
                    {email}
                </div>
            </Link>
            }
        </div>
    )
};
