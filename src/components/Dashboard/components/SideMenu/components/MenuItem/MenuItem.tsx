import React, { FC } from 'react';
import { Link /*, useRouteMatch*/ } from 'react-router-dom';
import styles from './MenuItem.module.sass';
import { trans } from '../../../../../../translations';

interface MenuItemProps {
  path: string;
  menuIcon: any;
  label: any;
  open: boolean;
  active?: boolean;
}

export const MenuItem: FC<MenuItemProps> = (props) => {
  const { path, active, menuIcon, label, open } = props;
  const title = trans(label);

  const isActive = () => {
    if (path === '/dashboard') {
      return window.location.pathname === path
    } else {
      return window.location.pathname.startsWith(path)
    }
  };

  return (
    <li
      className={`${styles.menuItem} ${isActive() || active ? styles.active : ''}`}
      title={title}
    >
      <Link to={path}>
        <img src={menuIcon} style={{ width: '18px', height: '18px' }} alt="menuIcon" />
        {open && <span>{title}</span>}
      </Link>
    </li>
  );
};

export default MenuItem;
