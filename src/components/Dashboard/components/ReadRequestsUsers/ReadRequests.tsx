import React, {FC, useEffect, useRef, useState} from 'react'
import {
    AppBar,
    AppBarBefore,
    AppBarAfter,
    Pagination
} from '../../../common/AppBar'
import {THead, TBody} from '../../../common/Table'
import {Button, TableCell, TableRow} from '@material-ui/core'
import {trans} from '../../../../translations'
import SearchField, {SearchFieldRef} from '../../../common/SearchField';
import ReadRequestItem from './components/ReadRequestItem/index'
import {PER_PAGE} from '../../../../data/constants';
import Loading from "../../../common/Loading";
import {CreateReadRequest} from './components/CreateReadRequest/CreateReadRequest';
import useReadRequest from "../../../../hooks/useReadRequest";
import useAuth from "../../../../hooks/useAuth";

export const ReadRequests: FC = () => {
    const [loading, setLoading] = useState(false);
    const [read, setRead] = useState(false);

    const {user} = useAuth();

    const {page, perPage, search, getReadRequests, readRequests = [], total} = useReadRequest();

    const searchRef = useRef<SearchFieldRef>(null);

    const handleRead = () => {
        setRead(!read);
    };

    useEffect(() => {
        setLoading(true);
        getReadRequests({
            perPage: perPage || PER_PAGE,
            page,
            search,
        }).catch(console.error).then(() => setLoading(false));
    }, []);

    const pageChanged = (nextPage: number) => {
        setLoading(true);
        getReadRequests({
            page: nextPage,
            perPage: perPage || PER_PAGE,
            search: search,
        }).catch(console.error).then(() => setLoading(false));
    };

    const handleSearchChange = (search: string) => {
        setLoading(true);
        getReadRequests({
            page: 1,
            perPage: perPage || PER_PAGE,
            search: search
        }).catch(console.error).then(() => setLoading(false));
    };

    return (
        <div className="dashboard-content">
            <Loading loading={loading}/>
            <AppBar title="menu.item.users">
                <AppBarBefore>
                    <SearchField ref={searchRef} defaultValue={search} onChange={handleSearchChange}/>
                    {user?.role === 'user' &&
                    <Button color="secondary" onClick={() => handleRead()}>Add Read Request</Button>
                    }
                </AppBarBefore>
                <AppBarAfter>
                    <Pagination currentPage={page!} perPage={perPage!}
                                totalPages={Math.ceil((total || 1) / (perPage || PER_PAGE))}
                                handleChange={pageChanged}/>
                </AppBarAfter>
            </AppBar>

            <THead>
                <TableRow>

                    <TableCell className='width-4' align="left">
                        {trans('table.header.user')}
                    </TableCell>
                    <TableCell className='width-3' align="left">
                        {trans('table.header.date')}
                    </TableCell>
                    <TableCell className='width-4'>
                        {trans('table.header.email')}
                    </TableCell>

                </TableRow>
            </THead>

            <TBody>
                {
                    readRequests.map((r, i) => {
                        return < ReadRequestItem
                            key={i}
                            readRequest={r}
                        />
                    })
                }
            </TBody>
            <CreateReadRequest open={read} handleClose={handleRead}/>
        </div>
    );
};
