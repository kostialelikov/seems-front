import React, {FC} from 'react'
import {AppDialog, AppDialogTitle, AppDialogContent} from '../../../../../common/Dialog/index'
import Form from './Form/Form'

interface Props {
  open: boolean
  handleClose: () => void
}

export const CreateReadRequest: FC<Props> = (props) => {

  const {open, handleClose} = props;

  return (
    <AppDialog
      open={open}
      handleClose={handleClose}
    >
      <AppDialogTitle
        handleClose={handleClose}
        title='app.bar.add.agency.button'/>
      <AppDialogContent>
        <Form handleClose={handleClose}/>
      </AppDialogContent>
    </AppDialog>
  )
}

