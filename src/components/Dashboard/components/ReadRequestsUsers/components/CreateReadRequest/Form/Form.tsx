import React, {FC} from 'react'
import {trans} from "../../../../../../../translations"
import TextField from '../../../../../../common/TextField';
import {Field, FormErrors, InjectedFormProps, reduxForm} from "redux-form";
import styles from "./From.module.sass";
import FormHelperText from "@material-ui/core/FormHelperText";
import Button from "@material-ui/core/Button";
import {PER_PAGE} from "../../../../../../../data/constants";
import useReadRequest from "../../../../../../../hooks/useReadRequest";

interface Props {
    handleClose: () => void
}

interface FormData {
    firstName: string;
    lastName: string;
    middleName: string;
    id: string;
}

const Form: FC<InjectedFormProps<FormData, any, any> & Props> = (props) => {

    const {createReadRequests, getReadRequests, page, search, perPage} = useReadRequest();

    const handleAddAgency = props.handleSubmit((formData: FormData) => {
        return createReadRequests(formData).then(() => {
            return getReadRequests({
                perPage: perPage || PER_PAGE,
                page,
                search
            });
        }).then(handleClose).catch(console.error);
    });

    const handleFieldChange = () => {
        const {clearSubmitErrors} = props as any

        return clearSubmitErrors && clearSubmitErrors()
    }

    const {error, submitFailed, handleClose, submitting} = props

    return (
        <div className="dialog-form">
            <div className="flexed-row">
                <div className="half-column">
                    <Field
                        component={TextField}
                        name={'firstName'}
                        label={trans('input.label.owner.first.name')}
                        placeholder={trans('input.placeholder.please.enter')}
                        onChange={handleFieldChange}
                    />
                </div>
                <div className="half-column">
                    <Field
                        component={TextField}
                        name={'lastName'}
                        label={trans('input.label.owner.last.name')}
                        placeholder={trans('input.placeholder.please.enter')}
                        onChange={handleFieldChange}
                    />
                </div>
            </div>
            <div className="flexed-row">
                <Field
                    component={TextField}
                    name={'middleName'}
                    label={trans('input.label.owner.middle.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    onChange={handleFieldChange}
                />
            </div>
            <div className="flexed-row">
                <Field
                    component={TextField}
                    name={'id'}
                    label={trans('input.label.id')}
                    placeholder={trans('input.placeholder.please.enter')}
                    onChange={handleFieldChange}
                />
            </div>
            <div className={`${styles.errorWrapper}`}>
                {(error && submitFailed) && (
                    <FormHelperText error>{trans(error)}</FormHelperText>
                )}
            </div>
            <div className="dialog-action">
                <Button
                    color='secondary'
                    variant='contained'
                    disabled={submitting}
                    onClick={handleAddAgency}
                >{trans('button.create')}</Button>
            </div>
        </div>
    )
}

const validate = (values: FormData): FormErrors<FormData, any> => {
    const {
        firstName,
        lastName,
        middleName,
        id
    } = values;

    const errors: any = {};

    if (!firstName
        && !lastName
        && !middleName
        && !id) {
        errors._error = trans('errors.at.least.one.required');
    }


    return Object.keys(errors).length !== 0 ? errors : null;
}


export default reduxForm<FormData, any, any>({
    form: 'CreateReadRequestForm',
    validate
})(Form);
