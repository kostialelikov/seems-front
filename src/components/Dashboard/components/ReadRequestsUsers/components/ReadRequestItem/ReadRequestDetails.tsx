import * as React from 'react'
import {Table, TableBody, TableHead, TableCell, TableRow, Button} from '@material-ui/core'
import '../../../../../../styles/dashboard.sass'
import {trans} from "../../../../../../translations";
import {ReadRequestInterface} from "../../../../../../interfaces/read_request";
import useAuth from "../../../../../../hooks/useAuth";
import useReadRequest from "../../../../../../hooks/useReadRequest";
import {PER_PAGE} from "../../../../../../data/constants";

interface Props {
    readRequest: ReadRequestInterface
}

const ReadRequestDetails: React.FC<Props> = (props) => {
    const {readRequest} = props;
    const {user} = useAuth();
    const {approveReadRequests, rejectReadRequests, getReadRequests, perPage, page, search} = useReadRequest();

    const handleApprove = () => {
        approveReadRequests(readRequest._id).then(() => {
            return getReadRequests({
                perPage: perPage || PER_PAGE,
                page,
                search
            })
        }).catch(console.warn)
    }

    const handleReject = () => {
        rejectReadRequests(readRequest._id).then(() => {
            return getReadRequests({
                perPage: perPage || PER_PAGE,
                page,
                search
            })
        }).catch(console.warn)
    }

    return (
        <Table className='expand-details-table'>
            {user?.role === 'user'
                ? <TableHead>
                    <TableRow>
                        <TableCell className='expand-th width-6'>{trans('table.header.name')}</TableCell>
                        <TableCell className='expand-th width-6'>{trans('table.header.value')}</TableCell>
                    </TableRow>
                </TableHead>
                : <TableHead>
                    <TableRow>
                        <TableCell className='expand-th width-12'>{trans('table.header.action')}</TableCell>
                    </TableRow>
                </TableHead>
            }
            <TableBody>
                {user?.role === 'user'
                    ? <div>
                        {
                            readRequest.target && readRequest.target[0]
                                ? Object.keys(readRequest.target[0]).map((s, i) => (
                                    <TableRow key={i}>
                                        <TableCell className='expand-th width-6'>{trans(`sremp.key.${s}`)}</TableCell>
                                        <TableCell
                                            className='expand-th width-6'>{(readRequest.target![0] as unknown as any)[s]}</TableCell>
                                    </TableRow>
                                ))
                                : <TableRow>
                                    <TableCell className='expand-th width-6'>No Data</TableCell>
                                    <TableCell className='expand-th width-6'>No Data</TableCell>
                                </TableRow>
                        }
                    </div>
                    :
                    <div>
                        <TableRow>
                            <TableCell className='expand-th width-6'>Requested by {readRequest.reporter[0].firstName} {readRequest.reporter[0].lastName}</TableCell>
                        </TableRow>
                        {
                            readRequest.status === 'pending'
                                ? <TableRow>
                                    <Button onClick={handleApprove} color="secondary">Approve</Button>
                                    <Button onClick={handleReject} color="secondary" variant="outlined">Reject</Button>
                                </TableRow>
                                : 'Already submitted'
                        }
                    </div>
                }
            </TableBody>
        </Table>
    )
}

export default ReadRequestDetails


