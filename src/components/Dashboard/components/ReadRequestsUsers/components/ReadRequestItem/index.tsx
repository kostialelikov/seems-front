import React, {FC} from 'react'
import '../../../../../../styles/dashboard.sass'
import {
    TableCell,
    TableRow,
    TableBody,
    Table
} from '@material-ui/core'
import moment from "moment";
import {StatusLabel} from '../StatusLabel';
import {ReadRequestInterface} from "../../../../../../interfaces/read_request";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import arrow from "../../../../../../assets/icons/ico-dropdown.svg";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ReadRequestDetails from './ReadRequestDetails';


/*
 Action Log Page layout the same for User/Admin/Owner
 The visual difference is the Title text
    For Admin and Owner: title='app.bar.title.actions.log'
    For User:  title='app.bar.title.my.actions.log'
*/

interface Props {
    readRequest: ReadRequestInterface
    // handleActions: () => void
}

const UsersItem: FC<Props> = (props) => {
    const {readRequest} = props;

    // eslint-disable-next-line no-restricted-globals
    const firstName = readRequest.filters.firstName,
        lastName = readRequest.filters.lastName,
        middleName = readRequest.filters.middleName,
        id = readRequest.filters.id
    const createdAt = readRequest.createdAt ? moment(readRequest.createdAt).format('MMMM DD, YYYY HH:MM') : "-";


    return (
        <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<img src={arrow} alt="img"/>}
            >
                <Table>
                    <TableBody>
                        <TableRow>
                            <TableCell
                                className='width-4'
                            >
                                <div className='user_with_avatar'>
                                    <div className="cell-overflow">
                                        {firstName || '-'} {middleName || '-'} {lastName || '-'}
                                    </div>
                                </div>
                            </TableCell>
                            <TableCell className='width-3'>
                                <div className="cell-overflow">{createdAt}</div>
                            </TableCell>
                            <TableCell className='width-4'>
                                <div className="cell-overflow">{readRequest.filters.id || 'id'}</div>
                            </TableCell>
                            <TableCell className='width-4'>
                                <StatusLabel status={readRequest.status}/>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <ReadRequestDetails readRequest={readRequest}/>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
};

export default UsersItem;
