import React, {FC} from 'react'
import moment from 'moment'
import {Link} from 'react-router-dom'
import {Table, TableBody, TableCell, TableRow, IconButton} from '@material-ui/core'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import '../../../../../styles/dashboard.sass'
import details from '../../../../../assets/icons/ico-file.svg'
import edit from '../../../../../assets/icons/ico-edit.svg'
import remove from '../../../../../assets/icons/ico-remove-active.svg'
import arrow from '../../../../../assets/icons/ico-dropdown.svg'
import SREMPDetails from './SREMPDetails'
import {SREMPInterface} from "../../../../../interfaces/sremp";

interface Props {
  sremp: SREMPInterface
  handleShowDetails: (e: any, sremp: SREMPInterface) => void
  handleShowDeleteDialog: (e: any, sremp: SREMPInterface) => void
}

const SREMPItem: FC<Props> = (props) => {
  const {sremp, handleShowDetails, handleShowDeleteDialog} = props

  return (
    <div className='paper-wrap'>
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<img src={arrow} alt="img"/>}
        >
          <Table>
            <TableBody>
              <TableRow>
                <TableCell className='width-3 expand-th'>
                  {moment(sremp.createdAt).format('lll')}
                </TableCell>
                <TableCell className='width-3 expand-th'>
                    {sremp.firstName} {sremp.lastName}
                </TableCell>
                <TableCell className='width-3 expand-th'>
                  {sremp._id}
                </TableCell>
                <TableCell className='width-2 expand-th'>
                  {sremp.encumbranceObject}
                </TableCell>
                <TableCell className='width-1 expand-th'>
                  <div className="actions with-expand">
                    <Link to={`/dashboard/create-sremp/step1/${sremp._id}`}>
                      <IconButton>
                        <img src={edit} alt="icon"/>
                      </IconButton>
                    </Link>
                    <IconButton
                      onClick={(e) => handleShowDeleteDialog(e, sremp)}
                    >
                      <img src={remove} alt="icon"/>
                    </IconButton>
                  </div>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <SREMPDetails sremp={sremp}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  )
}

export default SREMPItem