import React, {FC} from "react";
import {match, useRouteMatch, Redirect} from "react-router";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import {FormErrors, InjectedFormProps, reduxForm} from "redux-form";
import {trans} from "../../../../../../translations";
import {CreateSREMPFormData} from "../../../../../../interfaces/sremp";

export const SREMPForm: FC<InjectedFormProps<CreateSREMPFormData, any, any>> = (props) => {
    let {params} = useRouteMatch() as match<any>

    if (params.step === 'step1') {
        return <StepOne {...props} />
    } else if (params.step === 'step2') {
        return <StepTwo {...props} />
    } else {
        return <Redirect to="/dashboard"/>
    }
}

const validate = (values: CreateSREMPFormData): FormErrors<CreateSREMPFormData, any> => {
    const {
        firstName,
        lastName,
        middleName,
        encumbranceType,
        encumbranceModel,
        encumbranceObject,
        encumbranceReason,
        registrationType,
        registrationAgency,
        registrationDate
    } = values;

    let errors = {} as any;

    if (!(firstName && firstName.trim())) {
        errors.firstName = trans('errors.cannot.be.blank')
    }

    if (!(lastName && lastName.trim())) {
        errors.lastName = trans('errors.cannot.be.blank')
    }

    if (!(middleName && middleName.trim())) {
        errors.middleName = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceType && encumbranceType.trim())) {
        errors.encumbranceType = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceModel && encumbranceModel.trim())) {
        errors.encumbranceModel = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceObject && encumbranceObject.trim())) {
        errors.encumbranceObject = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceReason && encumbranceReason.trim())) {
        errors.encumbranceReason = trans('errors.cannot.be.blank')
    }


    if (!(registrationType && registrationType.trim())) {
        errors.registrationType = trans('errors.cannot.be.blank')
    }

    if (!(registrationAgency && registrationAgency.trim())) {
        errors.registrationAgency = trans('errors.cannot.be.blank')
    }

    if (!registrationDate) {
        errors.registrationDate = trans('errors.cannot.be.blank')
    }

    return errors
};

export default reduxForm<CreateSREMPFormData, any, any>({
    form: 'SREMPForm',
    validate
})(SREMPForm);

