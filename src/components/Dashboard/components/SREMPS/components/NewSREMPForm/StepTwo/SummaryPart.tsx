import React, {FC} from "react";
import {Link} from "react-router-dom";
import {IconButton} from "@material-ui/core";
import edit from "../../../../../../../assets/icons/ico-edit.svg";
import SummaryPartItem from './SummaryPartItem'

interface SummaryPartProps {
  title: string,
  editLink: string,
  items: any
}

const SummaryPart: FC<SummaryPartProps> = (props) => {
  const {title, editLink, items} = props

  return (
    <div>
      <h3 className='forms-subtitle'>
        <span>{title}</span>
        <Link to={editLink}>
          <IconButton className="edit-button">
            <img src={edit} alt="edit"/>
          </IconButton>
        </Link>
      </h3>

      {items.map((item: any, i: number) => (
        <SummaryPartItem
          key={i}
          item={item}
        />
      ))}
    </div>
  )
}

export default SummaryPart