import React, {FC} from "react";

interface SummaryPartItemProps {
  item: any
}

const SummaryPartItem: FC<SummaryPartItemProps> = ({item}) => {
  const {title, value} = item

  return (
    <div className='summary-row'>
      <div className='summary-row-label'>{title}</div>
      <div className='summary-row-content'>
        {value}
      </div>
    </div>
  )
}

export default SummaryPartItem