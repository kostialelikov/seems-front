import React, {FC, useState} from 'react'
import {Paper, Button} from '@material-ui/core'
import {Link, Redirect} from 'react-router-dom'
import moment from 'moment'
import {trans} from '../../../../../../../translations'
import arrow from '../../../../../../../assets/icons/ico-back-2.svg'
import '../styles.sass'
import useSREMP from "../../../../../../../hooks/useSREMP";
import SummaryPart from "./SummaryPart";
import {CreateSREMPFormData} from "../../../../../../../interfaces/sremp";
import FormHelperText from "@material-ui/core/FormHelperText";

export const StepTwo: FC<any> = (props) => {
    const {srempId} = props
    const {formValues, createSREMP, updateSREMP} = useSREMP()
    const [redirect, setRedirect] = useState(false)
    const [error, setError] = useState('')

    const handleCreateQuote = props.handleSubmit((formData: CreateSREMPFormData) => {
        if (formValues && formValues._id && srempId) {
            return updateSREMP(formData).then(() => {
                return setRedirect(true)
            }).catch((e) => {
                setError(e && e.errors && e.errors._error)
            })
        } else {
            return createSREMP(formData).then(() => {
                return setRedirect(true)
            }).catch((e) => {
                setError(e && e.errors && e.errors._error)
            })
        }
    })

    const isValid = () => {
        return formValues && formValues.firstName && formValues.lastName
            && formValues.middleName && formValues.middleName.trim()
            && formValues.firstName.trim() && formValues.lastName.trim()
            && formValues.encumbranceType && formValues.encumbranceType.trim()
            && formValues.encumbranceModel && formValues.encumbranceModel.trim()
            && formValues.encumbranceReason && formValues.encumbranceReason.trim()
            && formValues.encumbranceObject && formValues.encumbranceObject.trim()
            && formValues.registrationType && formValues.registrationType.trim()
            && formValues.registrationAgency && formValues.registrationAgency.trim()
            && formValues.registrationDate
    };

    return (
        <div className='scroll-container'>
            <div className='container container-big'>
                <Paper elevation={1}>
                    <div className='forms-container'>
                        <h2 className='forms-title'>{trans('title.summary')}</h2>
                        <div className="flexed-row flexed-row-start">
                            <div className="width-full">
                                <SummaryPart
                                    title={trans('form.quote.owner.information')}
                                    editLink={srempId ? `/dashboard/create-quote/step1/${srempId}` : `/dashboard/create-quote/step1`}
                                    items={[
                                        {title: trans('input.label.owner.first.name'), value: formValues.firstName},
                                        {title: trans('input.label.owner.last.name'), value: formValues.lastName},
                                        {title: trans('input.label.owner.middleName'), value: formValues.middleName}
                                    ]}
                                />

                                <SummaryPart
                                    title={trans('form.quote.encumbrance.information')}
                                    editLink={srempId ? `/dashboard/create-quote/step1/${srempId}#purchaseInformation` : `/dashboard/create-quote/step1#purchaseInformation`}
                                    items={[
                                        {
                                            title: trans('input.label.encumbrance.type'),
                                            value: formValues.encumbranceType
                                        },
                                        {
                                            title: trans('input.label.encumbrance.model'),
                                            value: formValues.encumbranceModel
                                        },
                                        {
                                            title: trans('input.label.encumbrance.reason'),
                                            value: formValues.encumbranceReason
                                        },
                                        {
                                            title: trans('input.label.encumbrance.object'),
                                            value: formValues.encumbranceObject
                                        }
                                    ]}
                                />

                                <SummaryPart
                                    title={trans('form.quote.registration.information')}
                                    editLink={srempId ? `/dashboard/create-quote/step1/${srempId}#purchaseInformation` : `/dashboard/create-quote/step1#purchaseInformation`}
                                    items={[
                                        {
                                            title: trans('input.label.registration.type'),
                                            value: formValues.registrationType
                                        },
                                        {
                                            title: trans('input.label.registration.agency'),
                                            value: formValues.registrationAgency
                                        },
                                        {
                                            title: trans('input.label.registration.date'),
                                            value: moment(formValues.registrationDate).format('MMMM DD, YYYY')
                                        },
                                    ]}
                                />
                            </div>
                        </div>
                    </div>
                    <div className='form-actions'>
                        <span className='form-prev-btn'>
                          <Link to={srempId ? `/dashboard/create-sremp/step1/${srempId}` : `/dashboard/create-sremp/step1`}>
                            <img src={arrow} alt="img"/>
                            <span>{trans('button.prev.step')}</span>
                          </Link>
                        </span>
                        <Button
                            onClick={handleCreateQuote}
                            variant='contained'
                            color='secondary'
                            disabled={props.submitting || !isValid()}
                        >
                            {trans(srempId ? 'button.edit.quote.confirm' : 'button.create.quote.confirm')}
                        </Button>
                        {error && (
                            <div className="error-wrapper">
                                {error && (
                                    <FormHelperText error>{error}</FormHelperText>
                                )}
                            </div>
                        )}
                    </div>
                </Paper>
            </div>

            {redirect && <Redirect to='/dashboard/sremps'/>}
        </div>
    )
}