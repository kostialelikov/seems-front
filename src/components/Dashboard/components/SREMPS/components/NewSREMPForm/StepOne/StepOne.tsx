import React, {FC, useState, useEffect, useRef} from 'react'
import {Paper, Button} from '@material-ui/core'
import {Redirect} from 'react-router'
import {trans} from '../../../../../../../translations'
import TextField from '../../../../../../common/TextField'
import SelectField from '../../../../../../common/SelectField'
import Switch from '../../../../../../common/Switch'
import DateField from '../../../../../../common/DateField'
import '../styles.sass'
import {Field, FieldArray} from "redux-form";
import useSREMP from "../../../../../../../hooks/useSREMP";
import anchorScrollContent from "../../../../../../../utils/anchorScrollContent";

export const StepOne: FC<any> = (props) => {
  const {srempId} = props;
  const [redirect, setRedirect] = useState(false);
  const [failed, setFailed] = useState(false);
  const {formValues} = useSREMP();

  const scrollEl = useRef(null);

  useEffect(() => {
    anchorScrollContent(scrollEl);
    document.addEventListener("wheel", function (event) {
      if (document.activeElement && (document.activeElement as any).type === "number") {
        (document.activeElement as any).blur();
      }
    });
  }, [])

  const handleNextStep = () => {
    const {
      firstName,
      lastName,
      middleName,
      encumbranceType,
      encumbranceModel,
      encumbranceObject,
      encumbranceReason,
      registrationType,
      registrationAgency,
      registrationDate
    } = formValues;

    let errors = {} as any;

    if (!(firstName && firstName.trim())) {
      errors.firstName = trans('errors.cannot.be.blank')
    }

    if (!(lastName && lastName.trim())) {
      errors.lastName = trans('errors.cannot.be.blank')
    }

    if (!(middleName && middleName.trim())) {
      errors.middleName = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceType && encumbranceType.trim())) {
      errors.encumbranceType = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceModel && encumbranceModel.trim())) {
      errors.encumbranceModel = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceObject && encumbranceObject.trim())) {
      errors.encumbranceObject = trans('errors.cannot.be.blank')
    }

    if (!(encumbranceReason && encumbranceReason.trim())) {
      errors.encumbranceReason = trans('errors.cannot.be.blank')
    }


    if (!(registrationType && registrationType.trim())) {
      errors.registrationType = trans('errors.cannot.be.blank')
    }

    if (!(registrationAgency && registrationAgency.trim())) {
      errors.registrationAgency = trans('errors.cannot.be.blank')
    }

    if (!registrationDate) {
      errors.registrationDate = trans('errors.cannot.be.blank')
    }

    if (Object.keys(errors).length > 0) {
      props.touch(...Object.keys(errors));
      setFailed(true);
    } else {
      setFailed(false);
      setRedirect(true);
    }
  }

  return (
    <div className='scroll-container' ref={scrollEl}>
      <div className='container'>
        <Paper elevation={1}>
          <div className='forms-container'>
            <h3 className='forms-subtitle'>{trans('form.quote.owner.information')}</h3>
            <div className='flexed-row'>
              <div className='third-column'>
                <Field
                  name="firstName"
                  component={TextField}
                  fullWidth
                  label={trans('input.label.owner.first.name')}
                  placeholder={trans('input.placeholder.please.enter')}
                  disabled={props.submitting}
                />
              </div>
              <div className='third-column'>
                <Field
                  name="lastName"
                  component={TextField}
                  fullWidth
                  label={trans('input.label.owner.last.name')}
                  placeholder={trans('input.placeholder.please.enter')}
                  disabled={props.submitting}
                />
              </div>
              <div className='third-column'>
                <Field
                    name="middleName"
                    component={TextField}
                    fullWidth
                    label={trans('input.label.owner.last.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    disabled={props.submitting}
                />
              </div>
            </div>
            <div className='flexed-row'>
              <div className='third-column'>
                <Field
                    name="encumbranceType"
                    component={TextField}
                    fullWidth
                    label={trans('input.label.owner.first.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    disabled={props.submitting}
                />
              </div>
              <div className='third-column'>
                <Field
                    name="encumbranceModel"
                    component={TextField}
                    fullWidth
                    label={trans('input.label.owner.last.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    disabled={props.submitting}
                />
              </div>
              <div className='third-column'>
                <Field
                    name="encumbranceReason"
                    component={TextField}
                    fullWidth
                    label={trans('input.label.owner.last.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    disabled={props.submitting}
                />
              </div>
            </div>
            <div className='flexed-row'>
              <div className='third-column'>
                <Field
                    name="encumbranceObject"
                    component={TextField}
                    fullWidth
                    label={trans('input.label.owner.first.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    disabled={props.submitting}
                />
              </div>
              <div className='third-column'>
                <Field
                    name="registrationType"
                    component={TextField}
                    fullWidth
                    label={trans('input.label.owner.last.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    disabled={props.submitting}
                />
              </div>
              <div className='third-column'>
                <Field
                    name="registrationAgency"
                    component={TextField}
                    fullWidth
                    label={trans('input.label.owner.last.name')}
                    placeholder={trans('input.placeholder.please.enter')}
                    disabled={props.submitting}
                />
              </div>
            </div>
            <div className='flexed-row'>
              <div className='width-full'>
                <Field
                    name="registrationDate"
                    component={DateField}
                    label={trans('input.label.owner.dob')}
                    disabled={props.submitting}
                />
              </div>
            </div>
          </div>
          <div className='form-actions'>
            <Button
              variant='contained'
              color='secondary'
              onClick={handleNextStep}
              // disabled={!stepIsValid()}
            >
              {trans('button.next.step')}
            </Button>
          </div>
          <div className="error">
            {failed && trans('errors.please.fill.all.fields')}
          </div>
        </Paper>
      </div>

      {redirect &&
      <Redirect to={srempId ? `/dashboard/create-sremp/step2/${srempId}` : `/dashboard/create-sremp/step2`}/>}
    </div>
  )
}