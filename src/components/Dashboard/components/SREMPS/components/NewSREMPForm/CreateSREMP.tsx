import React, {FC, useEffect, useState} from 'react'
import { match, useRouteMatch } from 'react-router'
import {trans} from '../../../../../../translations'
import {AppBar} from '../../../../../common/AppBar'
import back from '../../../../../../assets/icons/ico-back.svg'
import { Link } from 'react-router-dom'
import './styles.sass'
import SREMPForm from './SREMPForm'
import useSREMP from "../../../../../../hooks/useSREMP";
import Loading from '../../../../../common/Loading'

export const CreateSREMP: FC<any> = () => {
  let { params } = useRouteMatch() as match<any>
  const {sremp, getSREMP} = useSREMP()
  const srempId = params && params.srempId
  const [loading, setLoading] = useState(false);

  let changeSREMP
  if (sremp && sremp._id){
    changeSREMP = {
        ...sremp
    }
  }

  useEffect(() => {
    if (srempId){
      setLoading(true);
      getSREMP(srempId).catch(console.error).then(() => setLoading(false));
    }
  }, [])

  const getCurrentStep = () => {
    const step = params && params.step

    switch(step) {
      case `step1`:
        return 1
      case `step2`:
        return 2
      case `step3`:
        return 3
    }
  }

  const getBackButton = () => {
    const step = params && params.step

    switch(step) {
      case `step1`:
        return (
          <Link to={`/dashboard/quotes`}>
            <img src={back} alt='icon'/>
            <span>{trans('cancel.button')}</span>
          </Link>
        )
      case `step2`:
        return (
          <Link to={srempId ? `/dashboard/create-sremp/step1/${srempId}` : `/dashboard/create-sremp/step1`}>
            <img src={back} alt='icon'/>
            <span>{trans('step')} 1</span>
          </Link>
        )
    }
  }

  return (
    <div className='dashboard-content'>
      <AppBar title='title.create.quote'>
        <div className='bar-nav'>
          <div className='container'>
            {getBackButton()}
            <div className='form-steps'>{trans('step.of', {currentStep: getCurrentStep(), steps: 2})}</div>
          </div>
        </div>
      </AppBar>

      {(loading) ? (
        <Loading />
      ) : (
        <SREMPForm
          initialValues={changeSREMP}
          srempId={srempId}
        />
      )}
    </div>
  )
}
