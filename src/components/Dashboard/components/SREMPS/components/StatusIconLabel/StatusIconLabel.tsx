import React, { FC } from 'react'
import styles from './StatusIconLabel.module.sass'
import '../../../../../../styles/dashboard.sass'
//import {trans} from "../../../../../../translations";
import completed from '../../../../../../assets/icons/ico-completed.svg'
import failed from '../../../../../../assets/icons/ico-failed.svg'
import pending from '../../../../../../assets/icons/ico-pending.svg'
import progress from '../../../../../../assets/icons/ico-progress.svg'

interface Props {
  status: string
}

const statuses: any = {
  'Pending': pending,
  'pending': pending,
  'In Progress': progress,
  'in progress': progress,
  'Completed': completed,
  'completed': completed,
  'Failed': failed,
  'failed': failed,
  'success': completed,
  'Success': completed,
  'running': progress,
  'Running': progress,
  'error': failed,
  'Error': failed
}

const StatusIconLabel: FC <Props> = (props) => {
  const { status } = props;
  //const label = trans(`status.label.${status}`);

  return (
    <div className={styles.root}>
      <img src={statuses[status]} alt="ico"/>
      <p>{status}</p>
    </div>
  )
};

export default StatusIconLabel