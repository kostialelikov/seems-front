import React, {FC} from 'react'
import {Link} from 'react-router-dom'
import {Table, TableBody, TableHead, TableCell, TableRow} from '@material-ui/core'
import '../../../../../styles/dashboard.sass'
import {trans} from '../../../../../translations'
import StatusIconLabel from './StatusIconLabel/StatusIconLabel'
import {SREMPInterface} from "../../../../../interfaces/sremp";

interface Props {
  sremp: SREMPInterface
}

const SREMPDetails: FC<Props> = (props) => {
  const {sremp} = props;

  return (
    <Table className='expand-details-table'>
      <TableHead>
        <TableRow>
          <TableCell className='expand-th width-6'>{trans('table.header.name')}</TableCell>
          <TableCell className='expand-th width-6'>{trans('table.header.value')}</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {Object.keys(sremp).map((s, i) => (
          <TableRow key={i}>
            <TableCell className='expand-th width-6'>{trans(`sremp.key.${s}`)}</TableCell>
            <TableCell
              className='expand-th width-6'>{(sremp as unknown as any)[s]}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}

export default SREMPDetails


