import React, { FC } from 'react'
import Button from '@material-ui/core/Button'
import {trans} from '../../../../../../translations'
import {AppDialog} from '../../../../../common/Dialog'
import deleteIcon from '../../../../../../assets/icons/ico-trash.svg'
import {TableCell} from "@material-ui/core";
import {SREMPInterface} from "../../../../../../interfaces/sremp";

interface Props {
  open: boolean
  handleClose: () => void
  handleDelete: () => void
  sremp: SREMPInterface
}

export const DeleteDialog: FC<Props> = (props) => {
  const {open, handleClose, handleDelete, sremp} = props

  return(
    <AppDialog
      open={open}
      handleClose={handleClose}
    >
      <div className='delete-container'>
        <img src={deleteIcon} alt=''/>
        <h3 className='delete-title'>{trans('delete.quote.title')}</h3>
        <div className='agency-info'>
          <div className='agency-details'>
            {sremp.firstName}, {sremp.lastName}
            <p>{sremp._id}</p>
          </div>
        </div>
        <Button
          variant='contained'
          onClick={handleDelete}
          className='delete-button'
        >{trans('delete.button')}</Button>
        <Button
          onClick={handleClose}
          className='cancel-button'
        >{trans('cancel.button')}</Button>
      </div>
    </AppDialog>
  )
};

export default DeleteDialog;

