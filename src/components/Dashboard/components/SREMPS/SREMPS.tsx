import React, {FC, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import Button from '@material-ui/core/Button'
import '../../../../styles/dashboard.sass'
import {
  AppBar,
  AppBarBefore,
  AppBarAfter,
  Pagination
} from '../../../common/AppBar'
import {THead} from '../../../common/Table'
import {
  TableCell,
  TableRow
} from '@material-ui/core'
import {trans} from '../../../../translations'
import SREMPItem from './components/SREMPItem'
import DeleteDialog from './components/DeleteDialog'
import SearchField from '../../../common/SearchField'
import Loading from '../../../common/Loading'
import {SREMPInterface} from "../../../../interfaces/sremp";
import useSREMP from "../../../../hooks/useSREMP";

export const PER_PAGE = 7

export const SREMPS: FC = () => {
  const [detailDialog, setOpen] = useState(false);
  const [deleteDialog, setDeleteDialog] = useState(false);
  const [activeSREMP, setActiveSREMP] = useState<SREMPInterface>();
  const {getSREMPs, page, search, perPage, total, sremps, deleteSREMP} = useSREMP()

  useEffect(() => {
    getSREMPs({perPage: PER_PAGE, page, search}).catch(console.error)
  }, [])

  const handlePageChange = (value: number) => {;
    getSREMPs({perPage: PER_PAGE, page: value, search}).catch(console.error);
  }

  const handleSearchChange = (value: string) => {
    getSREMPs({perPage: PER_PAGE, page: 1, search: value}).catch(console.error);
  };

  const handleShowDeleteDialog = (e: any, sremp: SREMPInterface) => {
    setDeleteDialog(true);
    setActiveSREMP(sremp)
    e.stopPropagation()
  };

  const cancelDelete = () => {
    setDeleteDialog(false);
  };

  const deleteSREMPSubmit = () => {
    deleteSREMP(activeSREMP!._id).then(() => {
      setDeleteDialog(false)
      return getSREMPs({
        perPage: PER_PAGE,
        page: (page === total! && sremps!.length === 1 && page !== 1) ? page - 1 : page,
        search,
      });
    }).catch((e: any) => console.log(e))
  }

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className='dashboard-content'>
      <AppBar title='menu.item.quotes'>
        <AppBarBefore>
          <SearchField onChange={handleSearchChange}/>
        </AppBarBefore>
        <AppBarAfter>
          <Pagination currentPage={page!} perPage={perPage!}
                      totalPages={Math.ceil((total || 1) / (perPage || PER_PAGE))} handleChange={handlePageChange}/>
          <Link to='/dashboard/create-sremp/step1'>
            <Button
              size='small'
              variant='contained'
              color='secondary'
            >
              {trans('button.create.quote')}
            </Button>
          </Link>
        </AppBarAfter>
      </AppBar>

      <THead>
        <TableRow>
          <TableCell className='width-3'>{trans('table.header.created')}</TableCell>
          <TableCell className='width-3'>{trans('table.header.customer.name')}</TableCell>
          <TableCell className='width-3'>{trans('table.header.id')}</TableCell>
          <TableCell className='width-2'>{trans('table.header.encumbrance.object')}</TableCell>
          <TableCell className='width-1'>{trans('table.header.actions')}</TableCell>
        </TableRow>
      </THead>
      {/*<TBody>*/}
      {sremps && sremps.length ? (
        <div className="scroll-container">
          {sremps.map((sremp, index) => {
            return (
              <SREMPItem
                key={index}
                sremp={sremp}
                handleShowDetails={handleShowDeleteDialog}
                handleShowDeleteDialog={handleShowDeleteDialog}
              />
            )
          })}
        </div>
      ) : (
        <div className="empty-list">
          {trans('empty.list')}
        </div>
      )}
      {/*</TBody>*/}

      {activeSREMP && (
        <DeleteDialog
          open={deleteDialog}
          handleClose={cancelDelete}
          handleDelete={deleteSREMPSubmit}
          sremp={activeSREMP}
        />
      )}
    </div>
  )
}
