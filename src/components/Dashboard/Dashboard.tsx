import React, {FC, useState} from 'react';
import {match, Redirect, Route, Switch, useRouteMatch} from 'react-router';
import '../../styles/dashboard.sass';
import MenuDrawer from './components/MenuDrawer';
import useAuth from '../../hooks/useAuth';
import {UserInterface} from '../../interfaces/user';
import AppBar from '@material-ui/core/AppBar';
import {IconButton, Toolbar} from '@material-ui/core';
import menuIcon from '../../assets/icons/ico-menu.svg';
import {ManageUsers} from "./components/ManageUsers/ManageUsers";
import {SREMPS} from "./components/SREMPS/SREMPS";
import {CreateSREMP} from "./components/SREMPS/components/NewSREMPForm/CreateSREMP";
import ReadRequests from "./components/ReadRequestsUsers";

export const Dashboard: FC = () => {
    let {path} = useRouteMatch() as match<any>;
    const {user} = useAuth();
    const [open, setOpen] = useState(true);
    const [width, setWidth] = React.useState(window.innerWidth);

    const handleResize = () => {
        setWidth(window.innerWidth);
    };

    React.useEffect(() => {
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    });

    if (width <= 950) {
        return (
            <div className="dashboard-page">
                <AppBar
                    position="static"
                    style={{
                        minWidth: '1000px',
                        backgroundColor: '#261a54'
                    }}
                >
                    <Toolbar>
                        <IconButton onClick={() => setOpen(!open)} edge="start" color="inherit" aria-label="menu">
                            <img src={menuIcon} alt="Menu"/>
                        </IconButton>
                    </Toolbar>
                    <MenuDrawer open={open} setOpen={setOpen}/>
                    <Switcher user={user!} path={path}/>
                </AppBar>
            </div>
        );
    } else {
        return (
            <div className="dashboard-page">
                <MenuDrawer open={open} setOpen={setOpen}/>
                <Switcher user={user!} path={path}/>
            </div>
        );
    }
};

interface SwitcherProps {
    user: UserInterface;
    path: string;
}

const Switcher: FC<SwitcherProps> = ({path}) => {
    return (
        <Switch>
            <Route path={`${path}/users`}>
                <ManageUsers/>
            </Route>
            <Route path={`${path}/sremps`}>
                <SREMPS/>
            </Route>
            <Route path={`${path}/read_requests`}>
                <ReadRequests/>
            </Route>
            <Route path={`${path}/create-sremp/:step/:srempId?`}>
                <CreateSREMP/>
            </Route>
            <Route path={`${path}/`}>
                <Redirect to={`${path}/users`}/>
            </Route>
            <Redirect path={`${path}/*`} to={path}/>
        </Switch>
    );
};
