import React, { FC } from 'react';
import { createStyles, withStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select, { SelectProps } from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import {v4} from 'uuid';
import { InputProps } from '../Input';
import { colors, fontSizes } from '../../../theme';

const BootstrapInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      marginBottom: 13
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #cfcfe1',
      padding: '16px 22px',
      '&:focus': {
        borderColor: '#b2b2e7',
        backgroundColor: '#fff',
        borderRadius: 4
      }
    }
  })
)(InputBase);

interface IStyles {
  classes: {
    root: string;
    inputLabel: string;
    menuPaper: string;
    itemRoot: string;
    itemSelected: string;
    input?: string;
  };
}

export type SelectFieldProps = InputProps & {
  items: string[];
  id?: string;
  label?: string;
  value?: string;
  additionalText?: string;
  additionalTextMethod?: string;
};

const SelectsFieldBase: FC<SelectFieldProps & IStyles> = (props) => {
  const {
    classes,
    items,
    additionalText,
    additionalTextMethod,
    id,
    label,
    /* meta,*/ input,
    inputProps,
    value
  } = props;
  const inputId = id || `id-${v4()}`;

  return (
    <FormControl className={classes.root}>
      <InputLabel shrink htmlFor={inputId} classes={{ formControl: classes.inputLabel }}>
        {label}
      </InputLabel>
      <Select
        {...input}
        {...(inputProps as SelectProps)}
        MenuProps={{
          classes: {
            paper: classes.menuPaper
          }
        }}
        value={value || input?.value}
        input={
          <BootstrapInput classes={{ input: classes.input }} {...input} {...(inputProps as InputProps)} id={inputId} />
        }
      >
        {items &&
          items.map((item: any, key: number) => {
            const val = typeof item === 'object' ? item.value : item;
            const label = typeof item === 'object' ? item.label : item;
            return (
              <MenuItem
                value={val}
                key={key}
                classes={{
                  root: classes.itemRoot,
                  selected: classes.itemSelected
                }}
              >
                {input && input.name === 'coverageC' && val === 0
                  ? '$'
                  : additionalText && additionalTextMethod === 'start' && additionalText}
                {label}
                {input && input.name === 'coverageC' && val === 0
                  ? ''
                  : additionalText && additionalTextMethod === 'end' && additionalText}
              </MenuItem>
            );
          })}
      </Select>
    </FormControl>
  );
};

const SelectField = withStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    inputLabel: {
      position: 'relative',
      transform: 'none',
      color: colors.label,
      '&.Mui-focused': {
        color: colors.label
      },
      fontSize: fontSizes.table,
      fontWeight: theme.typography.fontWeightMedium,
      marginBottom: 9,
      whiteSpace: 'pre'
    },
    menuPaper: {
      marginTop: 58,
      maxHeight: 300
    },
    itemRoot: {
      borderRadius: 3,
      padding: '5px 7px',
      color: '#70708f',
      margin: '0 7px',
      '&:hover': {
        color: '#fff',
        backgroundColor: '#6d5ce7 !important'
      }
    },
    itemSelected: {
      color: '#fff',
      backgroundColor: '#6d5ce7 !important',
      borderRadius: 3,
      padding: '5px 7px',
      margin: '0 7px'
    },
    input: {
    }
  })
)(SelectsFieldBase);

export default SelectField;
