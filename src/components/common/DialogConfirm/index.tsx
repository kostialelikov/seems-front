import React, { FC } from 'react'
import Button from '@material-ui/core/Button'
import {trans} from '../../../translations'
import {AppDialog} from '../Dialog'

interface Props {
  open: boolean
  handleClose: () => void
  handleConfirm: () => void
  title?: string
  icon?: any
}

export const ConfirmDialog: FC<Props> = (props) => {
  const {open, handleClose, handleConfirm, title, icon} = props

  return(
    <AppDialog
      open={open}
      handleClose={handleClose}
    >
      <div className='confirm-popup-container'>
        {icon && <img src={icon} alt=''/>}
        <h3 className='title'>{title || trans('dialog.confirm.title')}</h3>
        <Button
          variant='contained'
          onClick={handleConfirm}
          className='confirm-button'
        >{trans('label.yes')}</Button>
        <Button
          onClick={handleClose}
          className='cancel-button'
        >{trans('label.no')}</Button>
      </div>
    </AppDialog>
  )
};

export default ConfirmDialog;

