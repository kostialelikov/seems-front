import React, { FC } from 'react';
import clsx from 'clsx';
import {v4} from "uuid";
import {createStyles, makeStyles, Theme, withStyles} from '@material-ui/core/styles/index';
import Radio, { RadioProps } from '@material-ui/core/Radio';
import RadioGroup, { RadioGroupProps } from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import {InputProps} from "../Input";
import {trans} from '../../../translations'

const useStyles = makeStyles({
  root: {
    display: 'flex',
    height: 33,
    width: 80,
    borderRadius: 0,
    border: 'solid 1px #cfcfe1',
    position: 'relative',
    backgroundColor: '#fff',
    '&.leftRadio': {
      borderBottomLeftRadius: 3,
      borderTopLeftRadius: 3
    },
    '&.rightRadio': {
      borderBottomRightRadius: 3,
      borderTopRightRadius: 3
    }
  },
  icon: {
    '&:before': {
      content: '""',
    },
    '$root.Mui-focusVisible &': {},
    'input:hover ~ &': {},
    'input:disabled ~ &': {}
  },
  checkedIcon: {
    backgroundColor: '#6d5ce7',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    borderRadius: 2,
    '&:before': {
      display: 'block',
      width: 86,
      height: 43,
      borderRadius: 1,
      content: '""'
    },
    'input:hover ~ &': {}
  }
});

interface RadioStyles {
  additionalRootClass?: string
}

// Inspired by blueprintjs
function StyledRadio(props: RadioProps & RadioStyles) {
  const {additionalRootClass, ...other} = props
  const classes = useStyles();

  return (
    <Radio
      className={additionalRootClass ? clsx(classes.root, additionalRootClass) : classes.root}
      //disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...other}
    />
  );
}

interface SwitchStyles {
  classes: {
    root: string,
    label: string,
    group: string,
    radio: string
  }
}

export type SwitchProps = InputProps & RadioGroupProps & {
  meta: boolean,
  id?: string,
}

const SwitchBase: FC<SwitchProps & SwitchStyles> = (props) => {
  const { classes, id, input, disabled, ...inputProps } = props
  const inputId = id || `id-${v4()}`

  return (
    <FormControl className={classes.root}>
      <RadioGroup
        id={inputId}
        className={classes.group}
        {...inputProps}
        {...input}
      >
        <FormControlLabel
          value={"alltime"}
          checked={input && input.value === "alltime"}
          control={<StyledRadio additionalRootClass="leftRadio" />}
          label={trans('label.alltime')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
        <FormControlLabel
          value={"month"}
          checked={input && input.value === "month"}
          control={<StyledRadio />}
          label={trans('label.month')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
        <FormControlLabel
          value={"week"}
          checked={input && input.value === "week"}
          control={<StyledRadio />}
          label={trans('label.week')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
        <FormControlLabel
          value={"custom"}
          checked={input && input.value === "custom"}
          control={<StyledRadio additionalRootClass="rightRadio" />}
          label={trans('label.custom')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
      </RadioGroup>
    </FormControl>
  );
}

const Switch = withStyles((theme: Theme) => createStyles({
  root: {
    width: '100%',
    marginTop: 13,
    marginBottom: 13
  },
  label: {
    color: '#73738b',
    position: 'relative',
    fontSize: 13,
    transform: 'none',
    fontWeight: 500,
    margin: 0,
    marginBottom: 9
  },
  group: {
    flexFlow: 'row',
    paddingLeft: 11
  },
  radio: {
    marginRight: 10,
    '& .MuiFormControlLabel-label': {
      position: 'absolute',
      width: 100,
      textAlign: 'center',
      color: '#000'
    },
    '& .Mui-checked ~.MuiFormControlLabel-label': {
      color: '#fff'
    }
  }
}))(SwitchBase)

export default Switch