import React, { FC } from 'react';
import {v4} from "uuid";
import {createStyles, Theme, withStyles} from '@material-ui/core/styles/index';
import Radio from '../Radio';
import RadioGroup, { RadioGroupProps } from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import {InputProps} from "../Input";
import {trans} from '../../../translations'
import {FormHelperText} from "@material-ui/core";


interface RadioSwitchStyles {
  classes: {
    root: string,
    label: string,
    group: string
  }
}

export type RadioSwitchProps = InputProps & RadioGroupProps & {
  meta: boolean,
  options: string[],
  id?: string,
  label?: string
}

const RadioSwitchBase: FC<RadioSwitchProps & RadioSwitchStyles> = (props) => {
  const { classes, id, label, input, meta, disabled, options, ...inputProps } = props
  const inputId = id || `id-${v4()}`

  return (
    <FormControl className={classes.root}>
      <FormLabel className={classes.label}>{label}</FormLabel>
      <RadioGroup
        id={inputId}
        className={classes.group}
        {...inputProps}
        {...input}
      >
        {options.map((option, ind) => (
          <FormControlLabel
            key={`rad-${ind}`}
            value={option}
            // checked={input && input.value === option}
            control={<Radio />}
            label={trans(`radio.switch.${option.toLowerCase()}`)}
            disabled={disabled}
          />
        ))}
      </RadioGroup>
      {(meta && meta.touched && typeof meta.error === 'string') && (
        <FormHelperText error={meta && Boolean(meta.error)}>{meta && meta.error}</FormHelperText>
      )}
    </FormControl>
  );
}

const RadioSwitch = withStyles((theme: Theme) => createStyles({
  root: {
    width: '100%',
    marginBottom: 13
  },
  label: {
    color: '#73738b !important',
    position: 'relative',
    fontSize: 13,
    transform: 'none',
    fontWeight: 500,
    margin: 0,
    marginBottom: 9
  },
  group: {
    flexFlow: 'row',
    '&>label': {
      marginRight: 30,
    }
  }
}))(RadioSwitchBase)

export default RadioSwitch