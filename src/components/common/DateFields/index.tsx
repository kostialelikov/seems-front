import React, { FC } from 'react'
import { createStyles, Theme, withStyles } from '@material-ui/core/styles/index'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import DateInput, { DateInputProps } from '../DateInput'
import { colors, fontSizes } from '../../../theme'
import {v4} from 'uuid'
import {WrappedFieldMetaProps, WrappedFieldProps} from 'redux-form'
import { FormHelperText } from '@material-ui/core'

interface IStyles {
  classes: {
    root: string,
    input: string,
    inputLabel: string
  }
}

export type DateFieldProps = DateInputProps & Partial<WrappedFieldProps> & {
  id?: string
  label?: string
}

const DateFieldsBase: FC<DateFieldProps & IStyles> = (props) => {
  const { classes, id, label, meta, input, ...inputProps } = props
  const { touched, error } = meta as WrappedFieldMetaProps
  const inputId = id || `id-${v4()}`

  return (
    <FormControl className={classes.root}>
      <InputLabel shrink htmlFor={inputId} classes={{ formControl: classes.inputLabel }}>
        {label}
      </InputLabel>





      {(meta && touched && typeof error === 'string') && (
        <FormHelperText error={Boolean(error)}>{error}</FormHelperText>
      )}
    </FormControl>
  )
}

const StylesDateFields = withStyles((theme: Theme) => createStyles({
  root: {
    marginBottom: theme.spacing(1),
    width: '100%'
  },
  input: {
    'label + &': {
      marginTop: '0 !important'
    }
  },
  inputLabel: {
    position: 'relative',
    transform: 'none',
    color: colors.label,
    '&.Mui-focused': {
      color: colors.label
    },
    fontSize: fontSizes.table,
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: 9
  }
}))(DateFieldsBase)

export default StylesDateFields
