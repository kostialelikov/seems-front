import React, {FC, useState} from 'react';
import TextField, {TextFieldProps} from '../TextField';
import styles from './Autocomplete.module.sass';

type AutocompleteProps = TextFieldProps & {
  options: Array<any>,
  getOptionLabel: (option: any) => string,
  onOptionClick: (index: number) => void
}

const Autocomplete: FC<AutocompleteProps> = (props) => {
  const {options, getOptionLabel, onFocus, onBlur, onOptionClick, ...otherProps} = props;

  const [active, setActive] = useState(false);
  const currentOptions = options
    .map((el: any, index) => {
      return {
        index,
        value: getOptionLabel(el)
      };
    });

  const handleFocus = (e: any) => {
    setActive(true);
    if (onFocus) {
      onFocus(e);
    }
  };

  const handleBlur = (e: any) => {
    setActive(false);
    if (onBlur) {
      onBlur(e);
    }
  };


  return <div onFocus={handleFocus} onBlur={handleBlur}>
    <TextField {...otherProps}/>
    {active &&
    <div className={styles.optionsBox}>
      {
        currentOptions.map((el: any, index) => {
          return <div onMouseDown={() => onOptionClick(el.index)} className={styles.optionItem} key={index}>
            {el.value}
          </div>
        })
      }
    </div>
    }
  </div>
};

export default Autocomplete;