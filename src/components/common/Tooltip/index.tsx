import React, { FC } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade';
import Tooltip from '@material-ui/core/Tooltip';
import info from "../../../assets/icons/ico-info.svg";
import infoHover from "../../../assets/icons/ico-info-hover.svg";

export type SimpleTooltipProps = {
  title: string,
  placement?: string,
  classes?: any
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    infoIcon: {
      position: 'absolute',
      bottom: 30,
      right: -45,
      width: 20,
      height: 20,
      cursor: 'pointer',
      backgroundImage: `url(${info})`,
      '&:hover': {
        backgroundImage: `url(${infoHover})`,
      }
    },
    tooltip: {
      width: 260,
      borderRadius: 8,
      boxShadow: '0 0 3px 0 rgba(0, 0, 0, 0.08)',
      border: 'solid 1px rgba(0, 0, 0, 0.09)',
      backgroundColor: '#ffffff',
      color: '#000',
      padding: 20,
      fontSize: "normal"
    }
  })
);

const SimpleTooltip: FC<SimpleTooltipProps> = (props) => {
  const {title} = props
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Tooltip
        TransitionComponent={Fade}
        title={title}
        classes={{ tooltip: classes.tooltip }}
        placement="right"
      >
        <div className={classes.infoIcon} />
      </Tooltip>
    </div>
  );
}

export default SimpleTooltip