import React, { FC, useState } from 'react';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { DateRangePicker as Picker } from 'react-date-range';
import styles from './DateRangePicker.module.sass'

interface PickerProps {
  turnOff?: (data: any) => void
  isOpened: boolean
}



const DateRangePicker: FC<PickerProps> = ({
  isOpened = false,
  turnOff = () => { },
  }) => {

  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [key,] = useState(undefined);

  const onChange = (data: any) => {
    setStartDate(data.range1.startDate);
    setEndDate(data.range1.endDate);
  }
  return isOpened ? (
    <div className={styles.datePicker} onMouseLeave={() => turnOff({ startDate, endDate })}>
      <Picker
        ranges={[{
          startDate,
          endDate,
          key
        }]}
        onChange={onChange} />
    </div>
  ) : null;

}

export default DateRangePicker;