import React, { FC } from 'react';
import clsx from 'clsx';
import {v4} from "uuid";
import {createStyles, makeStyles, Theme, withStyles} from '@material-ui/core/styles/index';
import Radio, { RadioProps } from '@material-ui/core/Radio';
import RadioGroup, { RadioGroupProps } from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import /*Input,*/ {InputProps} from "../Input";
import {trans} from '../../../translations'
import {FormHelperText} from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    display: 'flex',
    height: 33,
    width: 80,
    borderRadius: 0,
    border: 'solid 1px #cfcfe1',
    position: 'relative',
    backgroundColor: '#fff',
    '&.leftRadio': {
      borderBottomLeftRadius: 3,
      borderTopLeftRadius: 3
    },
    '&.rightRadio': {
      borderBottomRightRadius: 3,
      borderTopRightRadius: 3
    }
  },
  icon: {
    '&:before': {
      content: '""',
    },
    '$root.Mui-focusVisible &': {},
    'input:hover ~ &': {},
    'input:disabled ~ &': {}
  },
  checkedIcon: {
    backgroundColor: '#6d5ce7',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    borderRadius: 2,
    '&:before': {
      display: 'block',
      width: 86,
      height: 43,
      borderRadius: 1,
      content: '""'
    },
    'input:hover ~ &': {}
  }
});

interface RadioStyles {
  additionalRootClass: string
}

// Inspired by blueprintjs
function StyledRadio(props: RadioProps & RadioStyles) {
  const {additionalRootClass, ...other} = props
  const classes = useStyles();

  return (
    <Radio
      className={additionalRootClass ? clsx(classes.root, additionalRootClass) : classes.root}
      //disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...other}
    />
  );
}

interface SwitchStyles {
  classes: {
    root: string,
    label: string,
    group: string,
    radio: string
  }
}

export type SwitchProps = InputProps & RadioGroupProps & {
  meta: boolean,
  id?: string,
  label?: string
}

const SwitchBase: FC<SwitchProps & SwitchStyles> = (props) => {
  const { classes, id, label, input, meta, disabled, ...inputProps } = props
  const inputId = id || `id-${v4()}`

  return (
    <FormControl className={classes.root}>
      <FormLabel className={classes.label}>{label}</FormLabel>
      <RadioGroup
        id={inputId}
        className={classes.group}
        {...inputProps}
        {...input}
      >
        <FormControlLabel
          value={"1"}
          checked={input && input.value === "1"}
          control={<StyledRadio additionalRootClass="leftRadio" />}
          label={trans('label.yes')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
        <FormControlLabel
          value={"0"}
          checked={input && input.value === "0"}
          control={<StyledRadio additionalRootClass="rightRadio" />}
          label={trans('label.no')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
      </RadioGroup>
      {(meta && meta.touched && typeof meta.error === 'string') && (
        <FormHelperText error={meta && Boolean(meta.error)}>{meta && meta.error}</FormHelperText>
      )}
    </FormControl>
  );
}

const Switch = withStyles((theme: Theme) => createStyles({
  root: {
    width: '100%',
    marginBottom: 13
  },
  label: {
    color: '#73738b !important',
    position: 'relative',
    fontSize: 13,
    transform: 'none',
    fontWeight: 500,
    margin: 0,
    marginBottom: 9
  },
  group: {
    flexFlow: 'row',
    paddingLeft: 11
  },
  radio: {
    marginRight: 10,
    '& .MuiFormControlLabel-label': {
      position: 'absolute',
      width: 100,
      textAlign: 'center',
      color: '#000'
    },
    '& .Mui-checked ~.MuiFormControlLabel-label': {
      color: '#fff'
    }
  }
}))(SwitchBase)

export default Switch