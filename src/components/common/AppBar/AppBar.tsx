import React, { FC } from 'react';
import { IconButton } from '@material-ui/core';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { colors, fontSizes } from '../../../theme';
import { trans } from '../../../translations';
import iconFilter from '../../../assets/icons/ico-filters.svg';
//import iconSearch from '../../../assets/icons/ico-search.svg'
import iconBack from '../../../assets/icons/ico-back-2.svg';
import iconFront from '../../../assets/icons/ico-front-2.svg';
import {Pagination as MaterialPaginator} from "@material-ui/lab";

interface Props {
  title: string;
  subtitle?: string;
  children?: any;
}

export const AppBar: FC<Props> = (props) => {
  const { title, children, subtitle } = props;
  const pageTitle = trans(title);
  const classes = useStyles();

  return (
    <div className={classes.appBar}>
      <h1 className={classes.title}>
        {pageTitle}
        {subtitle && <span className={classes.subtitle}>{subtitle}</span>}
      </h1>
      {children}
    </div>
  );
};

interface BeforeProps {
  children: any;
}

export const AppBarBefore: FC<BeforeProps> = (props) => {
  const { children } = props;
  const classes = useStyles();

  return <div className={classes.leftActions}>{children}</div>;
};

interface AfterProps {
  children: any;
}

export const AppBarAfter: FC<AfterProps> = (props) => {
  const { children } = props;
  const classes = useStyles();

  return <div className={classes.rightActions}>{children}</div>;
};

interface FiltersProps {
  handleFilter?: () => void;
}

export const Filters: FC<FiltersProps> = (props) => {
  const classes = useStyles();
  const { handleFilter } = props;

  return (
    <div className={classes.filters}>
      {/* filters component will be developed after the main Dashboard layout */}
      <IconButton onClick={handleFilter} className={classes.filterIcon}>
        <img src={iconFilter} alt="filterIcon" />
      </IconButton>
    </div>
  );
};

interface PaginationProps {
  currentPage: number;
  perPage: number;
  totalPages: number;
  handleChange: (nextPage: number) => void;
}

export const Pagination: FC<PaginationProps> = (props) => {
  const classes = useStyles();
  const { currentPage, totalPages, handleChange } = props;
  return (
    <div className={classes.pagination}>
      <MaterialPaginator
        onChange={(event, page) => handleChange(page)}
        count={totalPages}
        page={currentPage}
        defaultPage={1}
        siblingCount={0}
        size={'small'}/>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    filters: {},
    filterIcon: {
      width: 36,
      height: 36,
      padding: 10,
      borderRadius: 4,
      marginLeft: 12,
      background: colors.white,
      border: 'solid 1px rgba(0, 0, 0, 0.09)',
      boxShadow: '0 0 3px 0 rgba(0, 0, 0, 0.08)'
    },
    pagination: {
      display: 'flex',
      alignItems: 'center',
      marginRight: 12
    },
    appBar: {
      minHeight: 36,
      padding: '12px 0',
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'row',
      background: colors.white,
      position: 'relative',
      zIndex: 1,
      boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.14)'
    },
    leftActions: {
      left: 12,
      position: 'absolute',
      display: 'flex'
    },
    rightActions: {
      right: 12,
      position: 'absolute',
      display: 'flex'
    },
    title: {
      width: '100%',
      margin: 0,
      textAlign: 'center',
      fontSize: fontSizes.title,
      fontWeight: 500,
      color: '#000000'
    },
    subtitle: {
      fontSize: fontSizes.main,
      color: colors.label,
      display: 'block'
      // marginTop: -3
    }
  })
);
