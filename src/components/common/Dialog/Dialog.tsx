import React, { FC } from 'react'
import Dialog from '@material-ui/core/Dialog'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogContent from '@material-ui/core/DialogContent'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles/index'
import { colors, fontSizes } from '../../../theme'
import closeIcon from '../../../assets/icons/ico-close.svg'
import resetIcon from '../../../assets/icons/ico-reset.svg'
import {trans} from "../../../translations";

interface DialogProps {
  open: boolean
  children: React.ReactNode
  handleClose: () => void
}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: 26,
      zIndex: 1,
      boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.14)',
      minWidth: 520,
    },
    title:{
      margin: 0,
      fontWeight: 500,
      textAlign: 'center',
      fontSize: fontSizes.subTitle,
    },
    closeButton: {
      right: 26,
      top: 24,
      padding: 6,
      borderRadius: 2,
      position: 'absolute',
      border: '2px solid rgba(115, 115, 139, 0.3)'
    },
    resetButton:{
      top: 16,
      left: 20,
      padding: 6,
      position: 'absolute',
      border: 'none',
      '& > span > span':{
        fontSize: fontSizes.main,
        color: colors.label,
        fontWeight: 400,
        paddingLeft: 8
      },
      '&:hover':{
        background: 'transparent'
      }
    }
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  title: string
  handleClose?: () => void
  handleReset?: () => void
}

export const AppDialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { title, classes, handleClose, handleReset, ...other } = props;
  const dialogTitle = trans(title)

  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      {handleReset ? (
        <Button aria-label="reset" className={classes.resetButton} onClick={handleReset}>
          <img src={resetIcon} alt="icon"/>
          <span>{trans('filters.reset')}</span>
        </Button>
      ) : null}
      <Typography variant="h1" className={classes.title}>{dialogTitle}</Typography>
      {handleClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
          <img src={closeIcon} alt="icon"/>
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});


export const AppDialogContent = withStyles((theme: Theme) => createStyles({
  root:{
    minWidth: 520,
    backgroundColor: colors.lightGreyBg
  }
}))(MuiDialogContent)

export const AppDialog: FC<DialogProps> = (props) => {
  const {children, open, handleClose} = props

  return (
      <Dialog
        maxWidth="sm"
        open={open}
        onClose={handleClose}
        disableBackdropClick={true}
        PaperProps={{
          elevation: 2
        }}
      >
        {children}
      </Dialog>
  )
}
