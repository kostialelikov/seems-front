import React, {FC} from 'react'
import {FormSection, Field, FormSectionProps} from 'redux-form'
import Input from '../Input'
import styles from './DateGroupField.module.sass'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from "@material-ui/core/InputLabel";
import {FormHelperText} from "@material-ui/core";

interface Props {
  label: string,
  disabled?: boolean,
  name?: string
  error?: string
}

export const DateInputGroup: FC<Props & FormSectionProps<any>> = (props) => {
  const {label, disabled, name, error} = props;

  const handleChange = (e: any) => {
    const value = e.currentTarget.value
    if (value < 0){
      e.preventDefault()
    }
  }

  return (
    <FormControl className={styles.formGroup}>
      <InputLabel shrink classes={{ formControl: styles.label }}>
        {label}
      </InputLabel>
      <FormSection
        name={name}
      >
        <div className={styles.formRow}>
          <Field
            name='day'
            component={Input}
            type='number'
            disabled={disabled}
            placeholder='DD'
            min="1"
            max="31"
            onChange={handleChange}
          />
          <Field
            name='month'
            component={Input}
            type='number'
            disabled={disabled}
            placeholder='MM'
            min="1"
            max="12"
            onChange={handleChange}
          />
          <Field
            name='year'
            component={Input}
            type='number'
            disabled={disabled}
            placeholder='YYYY'
            min="1"
            max="12"
            //min={new Date().getFullYear()}
            onChange={handleChange}
          />
        </div>
      </FormSection>
      {(/*meta && touched && */typeof error === 'string') && (
        <FormHelperText error={Boolean(error)}>{error}</FormHelperText>
      )}
    </FormControl>
  )
};

export default DateInputGroup;