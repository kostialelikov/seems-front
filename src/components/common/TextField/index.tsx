import React, { FC } from 'react'
import { createStyles, Theme, withStyles } from '@material-ui/core/styles/index'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Input, { InputProps } from '../Input'
import { colors, fontSizes } from '../../../theme'
import {v4} from 'uuid'
//import { WrappedFieldMetaProps } from 'redux-form'
import { FormHelperText } from '@material-ui/core'

interface IStyles {
  classes: {
    root: string,
    input: string,
    inputLabel: string
  }
}

export type TextFieldProps = InputProps & {
  id?: string
  label?: string
}

const TextFieldBase: FC<TextFieldProps & IStyles> = (props) => {
  const { classes, id, label, meta, ...inputProps } = props
  //const {touched, error} = meta as WrappedFieldMetaProps
  const inputId = id || `id-${v4()}`

  return (
    <FormControl className={classes.root}>
      <InputLabel shrink htmlFor={inputId} classes={{ formControl: classes.inputLabel }}>
        {label}
      </InputLabel>
      <Input
        {...(inputProps as InputProps)}
        error={Boolean(meta && meta.touched && meta.error)}
        meta={meta}
        id={inputId}
        classes={{ root: classes.input }}
      />
      {(meta && meta.touched && typeof meta.error === 'string') && (
        <FormHelperText error={meta && Boolean(meta.error)}>{meta && meta.error}</FormHelperText>
      )}
    </FormControl>
  )
}

const TextField = withStyles((theme: Theme) => createStyles({
  root: {
    marginBottom: theme.spacing(1),
    width: '100%'
  },
  input: {
    'label + &': {
      marginTop: '0 !important'
    }
  },
  inputLabel: {
    position: 'relative',
    transform: 'none',
    color: colors.label,
    '&.Mui-focused': {
      color: colors.label
    },
    fontSize: fontSizes.table,
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: 9
  }
}))(TextFieldBase)

export default TextField
