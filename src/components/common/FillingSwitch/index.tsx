import React, { FC } from 'react';
import clsx from 'clsx';
import {v4} from "uuid";
import {createStyles, makeStyles, Theme, withStyles} from '@material-ui/core/styles/index';
import Radio, { RadioProps } from '@material-ui/core/Radio';
import RadioGroup, { RadioGroupProps } from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import {InputProps} from "../Input";
import {trans} from '../../../translations'

const useStyles = makeStyles({
  root: {
    display: 'flex',
    height: 33,
    flex: 1,
    borderRadius: 0,
    border: 'solid 1px #cfcfe1',
    position: 'relative',
    backgroundColor: '#fff',
    '&.leftRadio': {
      borderBottomLeftRadius: 3,
      borderTopLeftRadius: 3
    },
    '&.rightRadio': {
      borderBottomRightRadius: 3,
      borderTopRightRadius: 3
    }
  },
  icon: {
    '&:before': {
      content: '""',
    },
    '$root.Mui-focusVisible &': {},
    'input:hover ~ &': {},
    'input:disabled ~ &': {}
  },
  checkedIcon: {
    backgroundColor: '#6d5ce7',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    borderRadius: 2,
    width: '100%',
    '&:before': {
      display: 'block',
      width: 86,
      height: 43,
      borderRadius: 1,
      content: '""'
    },
    'input:hover ~ &': {}
  }
});

interface RadioStyles {
  additionalRootClass?: string
}

// Inspired by blueprintjs
function StyledRadio(props: RadioProps & RadioStyles) {
  const {additionalRootClass, ...other} = props
  const classes = useStyles();

  return (
    <Radio
      className={additionalRootClass ? clsx(classes.root, additionalRootClass) : classes.root}
      //disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...other}
    />
  );
}

interface SwitchStyles {
  classes: {
    root: string,
    label: string,
    group: string,
    radio: string
  }
}

export type SwitchProps = InputProps & RadioGroupProps & {
  meta: boolean,
  id?: string,
}

const SwitchBase: FC<SwitchProps & SwitchStyles> = (props) => {
  const { classes, id, input, disabled, ...inputProps } = props
  const inputId = id || `id-${v4()}`

  return (
    <FormControl className={classes.root}>
      <RadioGroup
        id={inputId}
        className={classes.group}
        {...inputProps}
        {...input}
      >
        <FormControlLabel
          value={"Single"}
          checked={input && input.value === "Single"}
          control={<StyledRadio additionalRootClass="leftRadio" />}
          label={trans('filling-status.single')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
        <FormControlLabel
          value={"Head of Household"}
          checked={input && input.value === "Head of Household"}
          control={<StyledRadio />}
          label={trans('filling-status.head')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
        <FormControlLabel
          value={"Married"}
          checked={input && input.value === "Married"}
          control={<StyledRadio />}
          label={trans('filling-status.married')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
        <FormControlLabel
          value={"Married, filing Separate"}
          checked={input && input.value === "Married, filing Separate"}
          control={<StyledRadio additionalRootClass="rightRadio" />}
          label={trans('filling-status.separate')}
          className={clsx(classes.radio)}
          disabled={disabled}
        />
      </RadioGroup>
    </FormControl>
  );
}

const Switch = withStyles((theme: Theme) => createStyles({
  root: {
    width: '100%',
    marginTop: 13,
    marginBottom: 13,
  },
  label: {
    color: '#73738b',
    position: 'relative',
    fontSize: 13,
    transform: 'none',
    fontWeight: 500,
    margin: 0,
    marginBottom: 9,

  },
  group: {
    flexFlow: 'row',
    paddingLeft: 11,
  },
  radio: {
    marginRight: 10,
    display: 'flex',
    flex: 1,
    position: 'relative',
'& .MuiFormControlLabel-label': {
      position: 'absolute',
      width: '100%',
      textAlign: 'center',
      color: '#000'
    },
    '& .Mui-checked ~.MuiFormControlLabel-label': {
      color: '#fff'
    }
  }
}))(SwitchBase)

export default Switch