import React, { FC } from 'react'
import MuiCheckbox, { CheckboxProps } from '@material-ui/core/Checkbox'
import { createStyles, Theme, withStyles } from '@material-ui/core'
import { WrappedFieldProps } from "redux-form";

const icon = (<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
  <rect width="21" height="21" x=".5" y=".5" fill="#FFF" fillRule="evenodd" stroke="#000" rx="3" />
</svg>)

const checkedIcon = (<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
  <g fill="none" fillRule="evenodd">
    <rect width="22" height="22" fill="#6d5ce7" rx="3" />
    <path stroke="#FFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 8l-6.875 6L6 11.273" />
  </g>
</svg>)

const minus = (
  <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
    <g fill="none" fill-rule="evenodd">
      <rect width="22" height="22" fill="#6D5CE7" rx="3" />
      <path fill="#FFF" d="M6 10H16V12H6z" />
    </g>
  </svg>
)

const CheckboxBase: FC<CheckboxProps & Partial<WrappedFieldProps>> = props => {
  const { input, indeterminate=false, checked=false } = props;
 
  return <MuiCheckbox {...input} {...props} color="primary"
    icon={icon}
    checkedIcon={checkedIcon}
    indeterminateIcon={minus}
    indeterminate={indeterminate}
    checked={(input && input.value) || checked}
  />
};

const Checkbox = withStyles((theme: Theme) => createStyles({}))(CheckboxBase)

export default Checkbox