import React, { FC } from "react";
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
      position: 'absolute',
      backgroundColor: '#000',
      opacity: 0.3,
      color: '#6d5ce7',
      transition: 'visibility 0.33s linear, opacity 0.33s linear',
      zIndex: 20
    },
    disabled: {
      visibility: 'hidden',
      opacity: 0
    }
  }),
);

interface LoadingProps {
  loading?: boolean
  className?: any;
}

const Loading: FC<LoadingProps> = (props) => {
  const classes = useStyles();
  const { loading, className } = props;

  return (
    <div className={[`${classes.root} ${loading ? '' : classes.disabled}`,className].join(' ')}>
      <CircularProgress style={{
        color: '#6d5ce7',
      }} size={72} />
    </div>
  )
}

export default Loading