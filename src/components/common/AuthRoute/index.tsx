import React, { FC } from 'react'
import { Redirect, Route, RouteProps } from 'react-router-dom'
import useAuth from '../../../hooks/useAuth'

export type AuthRouteProps = RouteProps & {
  redirect: string
  noAuth?: boolean
}

const AuthRoute: FC<AuthRouteProps> = (props) => {
  const { noAuth = false, redirect, children, component, ...rest } = props
  const auth = useAuth()

  return (
    <Route
      {...rest}
      render={({ location }) => auth.isAuthenticated === (!noAuth)
        ? (component ? React.createElement(component) : children)
        : (<Redirect to={{
          pathname: redirect,
          state: { from: location }
        }}/>)
      }
    />
  )
}

export default AuthRoute