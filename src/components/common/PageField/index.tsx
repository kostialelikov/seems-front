import React, {ChangeEventHandler, FC, forwardRef, Ref, useImperativeHandle, useState} from 'react'
import {createStyles, InputBase, InputBaseClassKey, Theme, withStyles} from '@material-ui/core'
import { trans } from '../../../translations'
import { fontSizes } from '../../../theme'
import _ from 'lodash'

interface IStyles {
  classes: {
    input: string,
    searchIcon: string,
    inputRoot: string
    inputInput: string
  }
}

export type PageFieldRef = {
  setValue: (value: number) => void
}

export type PageProps = {
  value?: number
  onChange?: (value: number) => void
  defaultValue?: number
  ref?: Ref<PageFieldRef>
}

const PageFieldBase: FC<PageProps & IStyles> = forwardRef(({classes, onChange, value , defaultValue}, ref) => {
  const [val, setVal] = useState(value || '');

  useImperativeHandle(ref, () => ({
    setValue: setVal
  }));

  const dispatchChange = _.debounce((value: number) => onChange && onChange(value), 300)

  const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    setVal(event.target.value);
    dispatchChange(Number(event.target.value))
  };

  return (
    <div className={classes.input}>
      <InputBase
        placeholder={trans('app.bar.page.label')}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        defaultValue={defaultValue}
        type="number"
        inputProps={{ 'aria-label': 'Page', min: '1', max: '100' }}
        onChange={handleChange}
        value={val}
      />
    </div>
  )
});

const PageField = withStyles((theme: Theme) => createStyles({
  input: {
    marginLeft: 0,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#f0f1f4',
    '&:hover': {
      backgroundColor: '#f0f1f4'
    },
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: 0,
      width: 'auto'
    }
  },
  searchIcon: {
    right: 10,
    width: 16,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    fontSize: fontSizes.table,
    padding: '10px 30px 10px 15px',
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 50
    }
  }
}))(PageFieldBase)

export default PageField