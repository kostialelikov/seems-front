import React, {FC} from 'react'
import img from '../../../assets/img/bg.jpg'
import styles from './CarrierInfo.module.sass'

interface Props {
  avatar?: any
  label?: string
}

const CarrierInfo: FC<Props> = (props) => {
  const {avatar, label} = props;

  return (
    <div className={styles.carrierInfo} title={label}>
      <div className={styles.logoWrapper} style={{backgroundImage: `url(${avatar || img})`}}>
        {/*<img src={avatar || img} alt="avatar" className={styles.logo} />*/}
      </div>
      {label && <span>{label}</span>}
    </div>
  )
}

export default CarrierInfo