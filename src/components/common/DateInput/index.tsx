import { FC, default as React, useState } from 'react';
import { InputBaseClassKey } from '@material-ui/core/InputBase';
import moment from 'moment';
import { createStyles, StandardProps, Theme, withStyles } from '@material-ui/core';
import { colors } from '../../../theme';
import { darken } from '@material-ui/core/styles';
import { WrappedFieldProps } from 'redux-form';
import clsx from 'clsx';

export interface DateInputProps
  extends StandardProps<
      React.HTMLAttributes<HTMLDivElement>,
      InputBaseClassKey,
      'onChange' | 'onKeyUp' | 'onKeyDown' | 'onBlur' | 'onFocus' | 'defaultValue'
    >,
    Partial<WrappedFieldProps> {
  defaultValue?: unknown;
  disabled?: boolean;
  error?: boolean;
  fullWidth?: boolean;
  id?: string;
  margin?: 'dense' | 'none';
  name?: string;
  readOnly?: boolean;
  required?: boolean;
  value?: unknown;
  onChange?: any; //React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
  onKeyDown?: React.KeyboardEventHandler<HTMLTextAreaElement | HTMLInputElement>;
  onKeyUp?: React.KeyboardEventHandler<HTMLTextAreaElement | HTMLInputElement>;
  onBlur?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  onFocus?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
}

interface DateInputStyles {
  classes: {
    root: string;
    error: string;
  };
}

const DateInputBase: FC<DateInputProps & DateInputStyles> = (props) => {
  const { classes, /*onFocus, onBlur,*/ onChange, value, error, disabled } = props;
  const [focused, setFocused] = useState(false);
  const date = value ? moment(value as string) : moment();
  //const [dateValue, setDateValue] = useState(date)

  //const inputEl = useRef<HTMLInputElement>(null);

  const handleDayChange = (e: any) => {
    const dateValue = date.set('date', e.currentTarget.value);
    onChange(dateValue);
  };

  const handleMonthChange = (e: any) => {
    if (e.currentTarget.value > 0 && e.currentTarget.value <= 12) {
      const dateValue = date.set('month', Number(e.currentTarget.value) - 1);
      onChange(dateValue);
    }
  };

  const handleYearChange = (e: any) => {
    if (e.currentTarget.value >= 1 && e.currentTarget.value <= 9999) {
      const dateValue = date.set('year', e.currentTarget.value);
      onChange(dateValue);
    }
  };

  const handleFocus: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement> = (e) => {
    setFocused(true);
    //e.target.value = dateValue.toISOString()
    //onFocus && onFocus(e)
  };

  const handleBlur: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement> = (e) => {
    setFocused(false);
    //e.target.value = dateValue.toISOString()
    //onBlur && onBlur(e)
  };

  return (
    <div className={clsx(classes.root, focused && classes.focused, error && classes.error)}>
      <input
        disabled={disabled}
        value={date.get('month') + 1}
        placeholder="MM"
        type="number"
        min="1"
        max="12"
        className={classes.input}
        onChange={handleMonthChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
      <input
        disabled={disabled}
        value={date.get('date')}
        placeholder="DD"
        type="number"
        min="1"
        max="31"
        className={classes.input}
        onChange={handleDayChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
      <input
        disabled={disabled}
        value={date.get('year')}
        placeholder="YYYY"
        type="number"
        min="1"
        max="9999"
        className={classes.input}
        onChange={handleYearChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
      {/*<input ref={inputEl} type="hidden" onChange={onChange}/>*/}
    </div>
  );
};

const DateInput = withStyles((theme: Theme) => {
  return createStyles({
    root: {
      display: 'inline-flex',
      position: 'relative',
      borderRadius: 3,
      backgroundColor: theme.palette.common.white,
      border: `solid 1px ${colors.border}`,
      transition: theme.transitions.create(['border-color']),
      marginBottom: 4,
      marginTop: 4,
      '& > *:not(:first-child)': {
        borderLeft: `solid 1px ${colors.border}`
      },
      '&:hover': {
        borderColor: darken(colors.border, theme.palette.action.hoverOpacity),
        '& > *:not(:first-child)': {
          borderColor: darken(colors.border, theme.palette.action.hoverOpacity)
        }
      }
    },
    input: {
      ...theme.overrides!.MuiInputBase!.input,
      // flex: 1,
      border: 'none',
      outline: 'none',
      padding: '16px 1px',
      flex: '0 0.3 33%',
      overflow: 'hidden',
      width: '33.33%',
      textAlign: 'center',
      '&:disabled': {
        borderColor: theme.palette.action.disabled,
        backgroundColor: theme.palette.action.disabledBackground,
        '&:hover': {
          borderColor: theme.palette.action.disabled
        }
      }
    },
    focused: {
      borderColor: theme.palette.action.active,
      '& > *:not(:first-child)': {
        borderColor: theme.palette.action.active
      },
      '&:hover': {
        borderColor: darken(theme.palette.action.active, theme.palette.action.hoverOpacity),
        '& > *:not(:first-child)': {
          borderColor: darken(theme.palette.action.active, theme.palette.action.hoverOpacity)
        }
      }
    },
    disabled: {
      borderColor: theme.palette.action.disabled,
      backgroundColor: theme.palette.action.disabledBackground,
      '&:hover': {
        borderColor: theme.palette.action.disabled
      }
    },
    error: {
      borderColor: colors.borderError,
      '& > *:not(:first-child)': {
        borderColor: colors.borderError
      },
      '&:hover': {
        borderColor: darken(colors.borderError, theme.palette.action.hoverOpacity),
        '& > *:not(:first-child)': {
          borderColor: darken(colors.borderError, theme.palette.action.hoverOpacity)
        }
      }
    }
  });
})(DateInputBase);

export default DateInput;
