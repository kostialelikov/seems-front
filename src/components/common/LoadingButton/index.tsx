import React, {FC} from "react";
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import {Button, ExtendButtonBase, ButtonTypeMap, ButtonProps} from "@material-ui/core";
import {trans} from "../../../translations";


interface LoadingProps extends ButtonProps{
  loading?: boolean;
  loaderColor?: string;
}

const Loading: FC<LoadingProps> = ({ children, loading, loaderColor = '#fff', ...props }) => {

  return (
    <Button {...props}>
      {loading ? (
        <CircularProgress style={{
          color: loaderColor,
          paddingLeft: 20, paddingRight: 20
        }} size={20}/>
      ) : (
        children
      )}

    </Button>

  )
}

export default Loading