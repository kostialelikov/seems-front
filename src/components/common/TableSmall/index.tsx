import React, { FC } from 'react';
import { createStyles, Theme, makeStyles, withStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableHead } from '@material-ui/core';
import { colors } from '../../../theme';

/* THead and TBody divided in two components with fixed header
   and scrollable body (and also because of specific table body cover styles).
   As a result it is necessary to set width(%) to each cell of the thead and tbody
*/

interface TableProps {
  children: any;
}

export const THead: FC<TableProps> = (props) => {
  const { children } = props;
  const classes = useStyles();

  return (
    <div className={classes.tHCover}>
      <Table className={classes.tHeader}>
        <TableHead>{children}</TableHead>
      </Table>
    </div>
  );
};

interface TBodyProps {
  children: any;
}

export const TBody: FC<TBodyProps> = (props) => {
  const { children } = props;
  const classes = useStyles();

  return (
    <div className={classes.tableBody}>
      <div className={classes.tBodyContainer}>
        <Table className={classes.tBody}>
          <TableBody>{children}</TableBody>
        </Table>
      </div>
    </div>
  );
};

export const StyledCell = withStyles((theme) =>
  createStyles({
    body: {
      borderBottom: 'none',
      padding: '0'
    },
    root: {
      position: 'relative',
      '&:after': {
        right: 0,
        bottom: -1,
        height: 1,
        display: 'block',
        content: '" "',
        position: 'absolute',
        width: 'calc(100% - 58px)',
        background: 'rgba(0, 0, 0, 0.14)'
      }
    }
  })
)(TableCell);

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tHCover: {
      width: '100%',
      display: 'flex',
      position: 'relative',
      background: '#fff',
      padding: '0',
      boxSizing: 'border-box',
      borderBottomWidth: 1,
      borderTopWidth: 0,
      borderLeft: 0,
      borderRight: 0,
      borderColor: 'rgba(0, 0, 0, 0.14)',
      borderStyle: 'solid'
    },
    tHeader: {
      height: 40,
      width: '100%',
      border: 'none'
    },
    tableBody: {
      'overflow-y': 'auto',
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'flex-start',
      background: '#fff'
    },
    tBodyContainer: {
      width: '100%',
      // margin: '20px',
      padding: '0',
      background: colors.white,
      borderRadius: 10,
      '& > table tr:last-child td': {
        borderBottom: 'none',
        '&:after': {
          display: 'none'
        }
      }
    },
    tBody: {
      tableLayout: 'fixed'
    }
  })
);
