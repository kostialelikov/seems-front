import React, {ChangeEventHandler, FC, forwardRef, Ref, useEffect, useImperativeHandle, useState} from 'react'
import {createStyles, InputBase, Theme, withStyles} from '@material-ui/core'
import {trans} from '../../../translations'
import iconSearch from '../../../assets/icons/ico-search.svg'
import {fontSizes} from '../../../theme'
import _ from 'lodash'

interface IStyles {
  classes: {
    search: string,
    searchIcon: string,
    inputRoot: string
    inputInput: string
  }
}

export type SearchFieldRef = {
  setValue: (value: string) => void
}

export type SearchFieldProps = {
  value?: string
  onChange?: (value: string) => void,
  defaultValue?: string
  ref?: Ref<SearchFieldRef>
}

const SearchFieldBase: FC<SearchFieldProps & IStyles> = forwardRef(({classes, onChange, value, defaultValue}, ref) => {

  const [val, setVal] = useState(value || '');

  useImperativeHandle(ref, () => ({
    setValue: setVal
  }));

  const dispatchChange = _.debounce((value: string) => onChange && onChange(value), 300)

  const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    setVal(event.target.value);
    dispatchChange(event.target.value)
  };

  return (
    <div className={classes.search}>
      <InputBase
        placeholder={trans('app.bar.search.label')}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        defaultValue={defaultValue}
        inputProps={{'aria-label': 'search'}}
        onChange={handleChange}
        value={val}
      />
      <div className={classes.searchIcon}>
        <img src={iconSearch} alt="icon"/>
      </div>
    </div>
  )
})

const SearchField = withStyles((theme: Theme) => createStyles({
  search: {
    marginLeft: 0,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#f0f1f4',
    '&:hover': {
      backgroundColor: '#f0f1f4'
    },
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: 0,
      width: 'auto'
    }
  },
  searchIcon: {
    right: 10,
    width: 16,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    fontSize: fontSizes.table,
    padding: '10px 30px 10px 15px',
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 200
    }
  }
}))(SearchFieldBase)

export default SearchField