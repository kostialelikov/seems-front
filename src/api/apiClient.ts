import {HttpInterface} from './httpAdapter';
import * as querystring from "querystring";
import {UserRoles} from "../constants";
import {CreateSREMPFormData, SREMPInterface} from "../interfaces/sremp";
import {ReadRequestFormData} from "../interfaces/read_request";

interface QueryParams {
    perPage?: number;
    page?: number;
    search?: string;
}

export default class ApiClient {
    constructor(protected http: HttpInterface) {
    }

    setAuth(token: string) {
        this.http.setBearerToken(token);
    }

    getHealthCheck() {
        return this.http.get('/health-check');
    }

    public signIn(email: string, password: string) {
        return this.http.post('/auth/sign-in', {
            email,
            password
        });
    }

    public getUsers(params: QueryParams) {
        const query = querystring.stringify(params as any);
        return this.http.get(`/user${query ? `?${query}` : ''}`)
    }

    public createRegisterer(params: {
        firstName: string;
        lastName: string;
        email: string;
        password: string;
    }) {
        return this.http.post('/user', {...params, role: UserRoles.REGISTERER})
    }

    public getSREMPs(params: QueryParams) {
        const query = querystring.stringify(params as any);
        return this.http.get(`/sremp${query ? `?${query}` : ''}`)
    }

    public createSREMP(data: CreateSREMPFormData) {
        return this.http.post('/sremp', data);
    }

    public updateSREMP(data: CreateSREMPFormData) {
        return this.http.put('/sremp/' + data._id, data);
    }

    public getSREMP(id: string) {
        return this.http.get('/sremp/' + id);
    }

    public deleteSREMP(id: string) {
        return this.http.delete('/sremp/' + id);
    }

    public getUser(id: string) {
        return this.http.get(`/user/${id}`);
    }

    public createReadRequest(data: ReadRequestFormData) {
        return this.http.post('/read_request', data);
    }

    public getReadRequests(params: QueryParams) {
        const query = querystring.stringify(params as any);
        return this.http.get(`/read_request${query ? `?${query}` : ''}`);
    }

    public approveReadRequest(id: string) {
        return this.http.put(`/read_request/approve/${id}`);
    }


    public rejectReadRequest(id: string) {
        return this.http.put(`/read_request/reject/${id}`);
    }

}

