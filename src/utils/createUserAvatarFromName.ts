/**
 * @function createUserAvatarFromName
 * @param obj - {firstName: string, lastName: string}
 */
function createUserAvatarFromName({firstName = '', lastName = ''}: {firstName: string, lastName: string}): string {
  if(typeof firstName !== 'string' || typeof lastName !== 'string')
    return 'UN'

  const firstLetter = firstName.trim().charAt(0) || 'U'
  const secondLetter = lastName.trim().charAt(0) || firstName.trim().charAt(1) || 'N'
  return `${firstLetter}${secondLetter}`.toUpperCase()
}

export default createUserAvatarFromName