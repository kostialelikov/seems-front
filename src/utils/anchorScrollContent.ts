import {MutableRefObject} from "react";

export default (scrollEl: MutableRefObject<any>, correction = 80 as number) => {
  const elementId = window.location.hash
  const element = elementId && document.getElementById(elementId.substr(1))
  if (element && scrollEl.current){
    // @ts-ignore TODO fix this record
    scrollEl.current.scrollTo(0, element.offsetTop - correction)
  }
}