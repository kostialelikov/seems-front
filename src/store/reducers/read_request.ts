import {handleActions} from 'redux-actions'
import {ActionType} from 'redux-promise-middleware'
import {createReadRequests, getReadRequests, approveReadRequests, rejectReadRequests} from '../actions'
import {AnyAction} from 'redux'
import {ReadRequestInterface} from "../../interfaces/read_request";

export type ReadRequestState = {
    readRequests?: ReadRequestInterface[],
    perPage?: number;
    page?: number;
    total?: number;
    search?: string;
}

const reducer = handleActions<Partial<ReadRequestState>, AnyAction>({
    [createReadRequests.toString()]: {
        [ActionType.Fulfilled]: (state) => {
            return {...state, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [approveReadRequests.toString()]: {
        [ActionType.Fulfilled]: (state) => {
            return {...state, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [rejectReadRequests.toString()]: {
        [ActionType.Fulfilled]: (state) => {
            return {...state, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [getReadRequests.toString()]: {
        [ActionType.Fulfilled]: (state, action) => {
            return {...state, error: null, ...action.payload}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    }
},{
    readRequests: [],
    total: 1,
    page: 1,
    search: ''
});

export default reducer;