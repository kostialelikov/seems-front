import {handleActions} from 'redux-actions'
import {ActionType} from 'redux-promise-middleware'
import {getUsers, createRegisterer} from '../actions'
import {AnyAction} from 'redux'
import {UserInterface} from "../../interfaces/user";

export type UserState = {
    error?: any,
    users?: UserInterface[],
    total?: number,
    page?: number,
    perPage?: number,
    search?: string
}

const reducer = handleActions<Partial<UserState>, AnyAction>({
    [getUsers.toString()]: {
        [ActionType.Fulfilled]: (state, action) => {
            return {...state, ...action.payload, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [createRegisterer.toString()]: {
        [ActionType.Fulfilled]: (state) => {
            return {...state, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    }
},{
    users: [],
    total: 1,
    page: 1,
});

export default reducer;