import {handleActions} from 'redux-actions'
import {ActionType} from 'redux-promise-middleware'
import {getSREMPs, createSREMP, updateSREMP, getSREMP, deleteSREMP} from '../actions'
import {AnyAction} from 'redux'
import {SREMPInterface} from "../../interfaces/sremp";

export type SREMPState = {
    error?: any,
    sremp?: SREMPInterface,
    sremps?: SREMPInterface[],
    total?: number,
    page?: number,
    perPage?: number,
    search?: string
}

const reducer = handleActions<Partial<SREMPState>, AnyAction>({
    [getSREMPs.toString()]: {
        [ActionType.Fulfilled]: (state, action) => {
            return {...state, ...action.payload, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [createSREMP.toString()]: {
        [ActionType.Fulfilled]: (state) => {
            return {...state, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [updateSREMP.toString()]: {
        [ActionType.Fulfilled]: (state) => {
            return {...state, error: null}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [getSREMP.toString()]: {
        [ActionType.Fulfilled]: (state, action) => {
            return {...state, error: null, ...action.payload}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    },
    [deleteSREMP.toString()]: {
        [ActionType.Fulfilled]: (state, action) => {
            return {...state, error: null, sremps: state.sremps?.filter(sremp => sremp._id !== action.payload.id)}
        },
        [ActionType.Rejected]: (state, action) => {
            return {...state, error: action.payload}
        }
    }
},{
    sremps: [],
    total: 1,
    page: 1,
});

export default reducer;