import { UserInterface } from '../../interfaces/user'
import { handleActions } from 'redux-actions'
import { ActionType } from 'redux-promise-middleware'
import {authLogin, authLogout} from '../actions'
import { AnyAction } from 'redux'

export type AuthState = {
  user?: UserInterface
  token?: string,
}

const stateToLocalStorage = (state: any): any => {
  const {token, user = {}} = state;
  if (typeof localStorage !== 'undefined') {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
  }
  return state
};

const reducer = handleActions<Partial<AuthState>, AnyAction>({
  [authLogin.toString()]: {
    [ActionType.Fulfilled]: (state, action) => {
      if (!action.payload.forceNewPassword) {
        return stateToLocalStorage({ ...state, ...action.payload })
      } else {
        return {...state, ...action.payload};
      }
    },
    [ActionType.Rejected]: (state, action) => {
      return { ...state, ...action.payload }
    }
  },
  [authLogout.toString()]: {
    [ActionType.Fulfilled]: () => {
      return stateToLocalStorage({})
    },
    [ActionType.Rejected]: (state, action) => {
      return { ...state, ...action.payload }
    }
  },
}, {});

export default reducer