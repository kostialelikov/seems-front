import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import auth, {AuthState} from './auth';
import user, {UserState} from "./user";
import sremp, {SREMPState} from "./sremp";
import readRequest, {ReadRequestState} from "./read_request";

export type StoreState = {
    form: any;
    auth: Partial<AuthState>;
    user: Partial<UserState>;
    sremp: Partial<SREMPState>;
    readRequest: Partial<ReadRequestState>;
};

const reducers = combineReducers<Partial<StoreState>>({
    form,
    auth,
    user,
    sremp,
    readRequest
});

export default reducers;
