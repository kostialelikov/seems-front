import { StoreState } from './reducers'
import api from "../api";

export default function preloadState(): Partial<StoreState> {
  let auth = undefined;

  try {
    const token = localStorage.getItem('token') as unknown as string;
    const user = localStorage.getItem('user') as unknown as string;
    auth = {
      token: token,
      user: JSON.parse(user)
    }
  } catch (e) { }

  if (auth) {
    api.setAuth(auth.token || '');
  }

  return { auth }
}
