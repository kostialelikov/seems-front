import {createAction} from "redux-actions";
import api from "../../api";


export const getUsers = createAction('USER/GET', async (data: {
    perPage: number,
    page?: number,
    search?: string,
}) => {
    const res = await api.getUsers(data);
    return ({
        ...res,
        ...data
    });
});

export const createRegisterer = createAction('USER/CREATE', async (params: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}) => {
    await api.createRegisterer(params);
});