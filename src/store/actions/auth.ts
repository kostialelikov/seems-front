import {createAction} from 'redux-actions'
import api from '../../api'

export const authLogin = createAction('AUTH/LOGIN', async (email: string, password: string) => {
    const auth = await api.signIn(email, password);
    let user: any;
    api.setAuth(auth.token);
    user = auth.user;
    return {...auth, user};
});

export const authLogout = createAction('AUTH/LOGOUT', async () => {
    api.setAuth('');
});