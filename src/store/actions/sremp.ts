import {createAction} from "redux-actions";
import api from "../../api";
import {CreateSREMPFormData, SREMPInterface} from "../../interfaces/sremp";


export const getSREMPs = createAction('SREMP/GET/ALL', async (data: {
    perPage: number,
    page?: number,
    search?: string,
}) => {
    const res = await api.getSREMPs(data);
    return ({
        ...res,
        ...data
    });
});

export const createSREMP = createAction('SREMP/CREATE', async (data: CreateSREMPFormData) => {
    await api.createSREMP(data);
});

export const updateSREMP = createAction('SREMP/UPDATE', async (data: CreateSREMPFormData) => {
    await api.updateSREMP(data);
});

export const getSREMP = createAction('SREMP/GET', async (id: string) => {
    const {sremp} = await api.getSREMP(id);
    return {sremp};
});

export const deleteSREMP = createAction('SREMP/DELETE', async (id: string) => {
    await api.deleteSREMP(id);
    return {id};
});