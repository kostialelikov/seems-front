import {createAction} from "redux-actions";
import api from "../../api";
import {ReadRequestFormData} from "../../interfaces/read_request";


export const createReadRequests = createAction('READ_REQUEST/CREATE', async (data: ReadRequestFormData) => {
    await api.createReadRequest(data);
});

export const approveReadRequests = createAction('READ_REQUEST/APPROVE', async (id: string) => {
    await api.approveReadRequest(id);
});

export const rejectReadRequests = createAction('READ_REQUEST/REJECT', async (id: string) => {
    await api.rejectReadRequest(id);
});

export const getReadRequests = createAction('READ_REQUEST/GET/ALL', async (data: {
    perPage: number,
    page?: number,
    search?: string,
}) => {
    const res = await api.getReadRequests(data);
    return ({
        ...res,
        ...data
    });
});