import React, {FC} from 'react'
import {Router, Redirect, Switch} from 'react-router-dom'
import useAuth from './hooks/useAuth'
import Dashboard from './components/Dashboard'
import Login from './components/Login'
import {Provider as StoreProvider} from 'react-redux'
import store from './store'
import ThemeProvider from '@material-ui/styles/ThemeProvider'
import theme from './theme'
import AuthRoute from './components/common/AuthRoute'
import {createBrowserHistory} from 'history';

export const history = createBrowserHistory();

const App: FC = () => {
    const {isAuthenticated} = useAuth();
    return (
        <Router history={history}>
            <Switch>
                <AuthRoute path="/dashboard" component={Dashboard} redirect="/login"/>
                <AuthRoute path="/login" component={Login} noAuth redirect="/dashboard"/>
                <Redirect exact path="/*" to={isAuthenticated ? '/dashboard' : '/login'}/>
            </Switch>
        </Router>
    )
};

const AppProvider: FC = () => (
    <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
            <App/>
        </StoreProvider>
    </ThemeProvider>
)

export default AppProvider
