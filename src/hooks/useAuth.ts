import {useDispatch, useSelector} from 'react-redux'
import {StoreState} from '../store/reducers'
import {AuthState} from '../store/reducers/auth'
import {authLogin, authLogout} from '../store/actions'

export type LoginFormData = {
    email: string,
    password: string
}

export type AuthInterface = Partial<AuthState> & {
    isAuthenticated: boolean,
    login: (email: string, password: string) => Promise<any>
    logout: () => Promise<any>,
}

export default function useAuth(): AuthInterface {
    const auth = useSelector<StoreState, AuthState>(state => state.auth as AuthState)
    const dispatch = useDispatch();

    return {
        ...auth,
        isAuthenticated: Boolean(auth && auth.token), /// TMP
        login: async (email: string, password: string) => {
            await dispatch(authLogin(email, password))
        },
        logout: async () => {
            await dispatch(authLogout())
        }
    }
}