import {useSelector} from 'react-redux'
import {StoreState} from '../store/reducers'
import {ReadRequestState} from "../store/reducers/read_request";
import {createReadRequests, getReadRequests, approveReadRequests, rejectReadRequests} from '../store/actions'
import useAsyncDispatch from "./useAsyncDispatch";
import {ReadRequestFormData} from "../interfaces/read_request";

export type ReadRequestInterface = Partial<ReadRequestState> & {
  createReadRequests: (data: ReadRequestFormData) => Promise<any>;
  getReadRequests: (params: {
    perPage: number,
    page?: number,
    search?: string,
  }) => Promise<any>;
  approveReadRequests: (id: string) => Promise<any>;
  rejectReadRequests: (id: string) => Promise<any>;
}

export default function useReadRequest(): ReadRequestInterface {
  const readRequest = useSelector<StoreState, ReadRequestState>(state => state.readRequest as ReadRequestState)
  const dispatch = useAsyncDispatch();

  return {
    ...readRequest,
    createReadRequests: (data: ReadRequestFormData) => dispatch(createReadRequests(data)),
    getReadRequests: (params: {
      perPage: number,
      page?: number,
      search?: string,
    }) => dispatch(getReadRequests(params)),
    approveReadRequests: (id: string) => dispatch(approveReadRequests(id)),
    rejectReadRequests: (id: string) => dispatch(rejectReadRequests(id)),
  }
}