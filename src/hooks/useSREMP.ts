import {useSelector} from 'react-redux'
import {StoreState} from '../store/reducers'
import {SREMPState} from "../store/reducers/sremp";
import {getSREMPs, createSREMP, updateSREMP, getSREMP, deleteSREMP} from '../store/actions'
import useAsyncDispatch from "./useAsyncDispatch";
import {CreateSREMPFormData, SREMPInterface as SREMP} from "../interfaces/sremp";
import {getFormValues} from "redux-form";

export type SREMPInterface = Partial<SREMPState> & {
  formValues: CreateSREMPFormData;
  getSREMPs: (data: {
    perPage: number,
    page?: number,
    search?: string
  }) => Promise<any>,
  createSREMP: (data: CreateSREMPFormData) => Promise<any>
  updateSREMP: (data: CreateSREMPFormData) => Promise<any>
  getSREMP: (id: string) => Promise<any>
  deleteSREMP: (id: string) => Promise<any>
}

export default function useSREMP(): SREMPInterface {
  const formValues = useSelector<StoreState, CreateSREMPFormData>(state => getFormValues('SREMPForm')(state) as CreateSREMPFormData)
  const sremp = useSelector<StoreState, SREMPState>(state => state.sremp as SREMPState)
  const dispatch = useAsyncDispatch();

  return {
    ...sremp,
    formValues,
    getSREMPs: (data: {
      perPage: number,
      page?: number,
      search?: string,
    }) => dispatch(getSREMPs(data)),
    createSREMP: (data: CreateSREMPFormData) => dispatch(createSREMP(data)),
    updateSREMP: (data: CreateSREMPFormData) => dispatch(updateSREMP(data)),
    getSREMP: (id: string) => dispatch(getSREMP(id)),
    deleteSREMP: (id: string) => dispatch(deleteSREMP(id))
  }
}