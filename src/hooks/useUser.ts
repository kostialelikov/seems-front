import {useSelector} from 'react-redux'
import {StoreState} from '../store/reducers'
import {UserState} from "../store/reducers/user";
import {getUsers, createRegisterer} from '../store/actions'
import useAsyncDispatch from "./useAsyncDispatch";

export type UserInterface = Partial<UserState> & {
  getUsers: (data: {
    perPage: number,
    page?: number,
    search?: string
  }) => Promise<any>
  createRegisterer: (data: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
  }) => Promise<any>
}

export default function useUser(): UserInterface {
  const user = useSelector<StoreState, UserState>(state => state.user as UserState)
  const dispatch = useAsyncDispatch();

  return {
    ...user,
    getUsers: (data: {
      perPage: number,
      page?: number,
      search?: string,
    }) => dispatch(getUsers(data)),
    createRegisterer: (data: {
      firstName: string;
      lastName: string;
      email: string;
      password: string;
    }) => dispatch(createRegisterer(data))
  }
}