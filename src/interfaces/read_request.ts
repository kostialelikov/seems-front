import {UserInterface} from "./user";
import {SREMPInterface} from "./sremp";

export interface ReadRequestFormData {
    firstName?: string;
    lastName?: string;
    middleName?: string;
    id?: string;
}

export interface ReadRequestInterface {
    _id: string;
    reporter: UserInterface[];
    target?: SREMPInterface[];
    status: 'approved' | 'pending' | 'rejected',
    filters: {
        firstName?: string;
        lastName?: string;
        middleName?: string;
        id?: string;
    };
    createdAt: Date | string;
}