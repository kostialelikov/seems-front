import {UserInterface} from "./user";

export interface SREMPInterface {
    _id: string;
    firstName: string;
    lastName: string;
    middleName: string;
    encumbranceType: string;
    encumbranceModel: string;
    encumbranceReason: string;
    encumbranceObject: string;
    registrationType: string;
    registrationDate: Date | string;
    registrationAgency: string;
    createdAt: Date | string;
    updatedAt: Date | string;
    registerer: UserInterface | string;
}

export interface CreateSREMPFormData {
    _id?: string;
    firstName: string;
    lastName: string;
    middleName: string;
    encumbranceType: string;
    encumbranceModel: string;
    encumbranceReason: string;
    encumbranceObject: string;
    registrationType: string;
    registrationDate: Date | string;
    registrationAgency: string;
}