export enum UserRoles {
  ADMIN = 'admin',
  REGISTER = 'register',
  USER = 'user'
}

export interface UserInterface {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  role: UserRoles;
  password: string;
  createdAt?: string;
}
