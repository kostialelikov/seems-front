import translation from './en/index.json'

type TranslationValues = {
  [name: string]: any
}

function replaceValues(source: string, values: TranslationValues) {
  if (values){
    const arr = source.split(' ')
    const formattedMessage = arr.map((word) => {
      const key = word.substring(1, word.length - 1)
      if (word.startsWith('{') && values[key]){
        return values[key]
      } else {
        return word
      }
    })
    return formattedMessage.join(' ')
  }
  return source
}

export function trans(key: string, values?: TranslationValues) {
  const text = (translation as {[key: string]: string})[key] || key

  return (values) ? replaceValues(text, values) : text
}