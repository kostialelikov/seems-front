export enum UserRoles {
  ADMIN = 'admin',
  REGISTERER = 'registerer',
  USER = 'user'
}
